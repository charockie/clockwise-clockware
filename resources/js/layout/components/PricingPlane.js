import React, {Component} from "react";
import PropTypes from "prop-types";

class PricingPlane extends Component {
    constructor(props) {
        super(props);

        this.choosePlane = this.choosePlane.bind(this);
    }
    static propTypes = {
        pricingPlans: PropTypes.arrayOf(
            PropTypes.shape({
                name: PropTypes.string,
                title: PropTypes.string,
                cost: PropTypes.number,
                duration: PropTypes.number,
                descriptions: PropTypes.arrayOf(PropTypes.string)
            })
        ).isRequired
    }
    choosePlane(clockType, event) {
        event.preventDefault();

        this.props.scrollToOrderForm(event, {clock_type: clockType});
    }
    render(){
        return (
            <section className="pricing-plane-area section_padding_100_70 clearfix" id="pricing">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/*Heading Text*/}
                            <div className="section-heading text-center">
                                <h2>Наши тарифы</h2>
                                <div className="line-shape"></div>
                            </div>
                        </div>
                    </div>

                    <div className="row no-gutters">
                        {this.props.pricingPlans.map((plane, i) =>
                            <div key={i.toString()} className="col-12 col-md-6 col-lg-4">
                                {/*Package Price*/}
                                <div className="single-price-plan text-center">
                                    {/*Package Text*/}
                                    <div className="package-plan">
                                        <h5>{plane.title}</h5>
                                        <div className="ca-price d-flex justify-content-center">
                                            <span>₴</span>
                                            <h4>{plane.cost}</h4>
                                        </div>
                                    </div>
                                    <div className="package-description">
                                        {plane.descriptions.map((description, k) =>
                                            <p key={k.toString()}>{description}</p>
                                        )}
                                    </div>
                                    {/*Plan Button*/}
                                    <div className="plan-button">
                                        <a onClick={this.choosePlane.bind(this, plane.name, event)}>Заказать ремонт</a>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </section>
        )
    }
}
export default PricingPlane;
