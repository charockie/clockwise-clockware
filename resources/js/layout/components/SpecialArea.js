import React, {Component} from "react";

class SpecialArea extends Component {
    render(){
        return (
            <section className="special-area bg-white section_padding_100_70" id="about">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/*Section Heading Area*/}
                            <div className="section-heading text-center">
                                <h2>Почему именно мы?</h2>
                                <div className="line-shape"></div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        {/*Single Special Area*/}
                        <div className="col-12 col-md-4">
                            <div className="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                                <div className="single-icon">
                                    <i className="ti-mobile" aria-hidden="true"></i>
                                </div>
                                <h4>Мы всегда на связи</h4>
                                <p>В любое время свяжитесь с нами и узнайте статус вашего шего заказа</p>
                            </div>
                        </div>
                        {/*Single Special Area*/}
                        <div className="col-12 col-md-4">
                            <div className="single-special text-center wow fadeInUp" data-wow-delay="0.4s">
                                <div className="single-icon">
                                    <i className="ti-ruler-pencil" aria-hidden="true"></i>
                                </div>
                                <h4>Диагностика</h4>
                                <p>Мы бесплатную диагностику для Ваших часов в кратчайшие сроки</p>
                            </div>
                        </div>
                        {/*Single Special Area*/}
                        <div className="col-12 col-md-4">
                            <div className="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                                <div className="single-icon">
                                    <i className="ti-settings" aria-hidden="true"></i>
                                </div>
                                <h4>Качестванная работа</h4>
                                <p>Наши мастера делают свою работу качественно и ответственно</p>
                            </div>
                        </div>
                    </div>
                </div>
                {/*Special Description Area*/}
                <div className="special_description_area mt-150">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="special_description_img">
                                    <img src="/images/special.png" alt="" />
                                </div>
                            </div>
                            <div className="col-lg-6 col-xl-5 ml-xl-auto">
                                <div className="special_description_content">
                                    <h2>У нас лучший сервис для Вас!</h2>
                                    <p>Встали часы, и новая батарейка не спасла? Не стоит их выбрасывать! В Часовой
                                        мастерской Clockwise Clockware определят, в чем причина поломки, отрегулируют,
                                        удалят следы коррозии, при необходимости сменят механизм.</p>
                                    <div className="app-download-area">
                                        <div className="app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                            {/*Google Store Btn*/}
                                            <a href="#">
                                                <i className="fa fa-android"></i>
                                                <p className="mb-0"><span>available on</span> Google Store</p>
                                            </a>
                                        </div>
                                        <div className="app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                                            {/*Apple Store Btn*/}
                                            <a href="#">
                                                <i className="fa fa-apple"></i>
                                                <p className="mb-0"><span>available on</span> Apple Store</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
export default SpecialArea;
