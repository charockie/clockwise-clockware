import React, {Component} from "react";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Preview from "./Preview";
import Success from "./Success";

class OrderForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            clockTypes: [],
            cities: [],
            masters: [],
            currentStep: 1,
            orderWasCreated: false,
            data: {
                email: "",
                name: "",
                comment: "",
                clock_type: "",
                city_id: "",
                master_id: "",
                datetime: "",
            },
            errors: {}
        };

        this.handleChange = this.handleChange.bind(this);
        this._next = this._next.bind(this);
        this._prev = this._prev.bind(this);
        this.loadData = this.loadData.bind(this);
        this.loadAvailableMasters = this.loadAvailableMasters.bind(this);
        this.fillData = this.fillData.bind(this);
        this.getDatetimeFrom = this.getDatetimeFrom.bind(this);
        this.getDatetimeTo = this.getDatetimeTo.bind(this);
    }
    fillData(data) {
        let stateData = this.state.data;

        Object.keys(data).forEach((key) => {
            stateData[key] = data[key];
        });

        stateData["master_id"] = "";
        if (this.state.currentStep >= 3)
            this.state.currentStep--;

        this.setState({data: stateData});
    }
    componentDidMount() {
        this.loadData();
    }
    loadData() {
        axios.get("/api/clocks")
            .then((response) => { this.setState({clockTypes: response.data.data}) })
            .catch((error) => { console.error("API error", [error]); });

        axios.get("/api/cities")
            .then((response) => { this.setState({cities: response.data.data}) })
            .catch((error) => { console.error("API error", [error]); });
    }
    loadAvailableMasters() {
        return axios.get("/api/masters", {
            params: {
                dateFrom: this.getDatetimeFrom(),
                clock_type: this.state.data.clock_type,
                city: this.state.data.city_id
            }
        })
            .then((response) => { this.setState({masters: response.data.data}) })
            .catch((error) => { console.error("API error", [error]); });
    }
    validate(success) {
        const data = {...this.state.data};
        data["step"] = this.state.currentStep;
        data["datetime"] = this.getDatetimeFrom();

        axios.post("/", data)
            .then(success)
            .catch((error) => {
                this.setState({errors: error.response.data.errors});
            });
    }
    _next() {
        let currentStep = this.state.currentStep;

        this.validate(promise => {
            if (currentStep === 2) {
                this.loadAvailableMasters().then((response) => {
                    if (this.state.masters.length > 0) {
                        currentStep = currentStep >= 2? 3: currentStep + 1;
                        this.setState({
                            currentStep: currentStep,
                            errors: {},
                        });
                    } else {
                        this.setState({
                            errors: {
                                masters: ["Нет свободного мастера на выбранное время."]
                            },
                        });
                    }
                });
            } else {
                currentStep = currentStep >= 2? 3: currentStep + 1;
                this.setState({
                    currentStep: currentStep,
                    errors: {},
                });
            }
        });
    }
    _prev() {
        let currentStep = this.state.currentStep;
        if (currentStep === 3) {
            let data = this.state.data;
            data["master_id"] = "";
            this.setState({data: data});
        }

        currentStep = currentStep <= 1? 1: currentStep - 1;
        this.setState({
            currentStep: currentStep
        });
    }
    handleChange = (event) => {
        const {name, value} = event.target;

        let data = this.state.data;
        data[name] = value;

        this.setState({data: data});
    }
    handleSubmit = (event) => {
        event.preventDefault();

        this.validate(response => {
            if (response.data.success === true) {
                let clearData = {};
                Object.keys(this.state.data).map((key, value) => { Object.assign(clearData, {[key]: ""}) });

                this.setState({
                    currentStep: 1,
                    orderWasCreated: true,
                    data: clearData,
                    errors: {}
                });
                setTimeout(() => { this.setState({orderWasCreated: false}) }, 10000);
            }
        });
    }
    getDatetimeFrom = () => {
        return this.state.data.datetime.toLocaleString();
    }
    getDatetimeTo = () => {
        let clockType = this.state.clockTypes.find(type => type.name === this.state.data.clock_type),
            dateTo;

        if (clockType) {
            dateTo = new Date(this.state.data.datetime);
            dateTo.setHours(dateTo.getHours() + clockType.duration);
        } else
            dateTo = this.state.data.datetime;

        return dateTo.toLocaleString();
    }
    get previousButton(){
        if(this.state.currentStep !==1){
            return (
                <button
                    className="btn btn-secondary"
                    type="button" onClick={this._prev}>
                    Предыдущий шаг
                </button>
            )
        }
        return null;
    }
    get nextButton() {
        if (this.state.currentStep < 3) {
            return (
                <button
                    className="btn btn-primary float-right"
                    type="button" onClick={this._next}>
                    Следующий шаг
                </button>
            )
        }
    }
    get submitButton() {
        if (this.state.currentStep >= 3) {
            return (
                <button
                    className="btn btn-success float-right"
                    type="submit" onClick={this.handleSubmit}>
                    Готово!
                </button>
            )
        }
    }
    render() {
        return (
            <section className="footer-contact-area section_padding_100 clearfix" id="contact">
                <div className="container">

                    { this.state.orderWasCreated ?
                        <Success/>
                        :
                        <form onSubmit={this.handleSubmit}>

                            <div className="row mb-2">
                                <div className="col-md-6">
                                    <Preview
                                        clockTypes={this.state.clockTypes}
                                        cities={this.state.cities}
                                        masters={this.state.masters}
                                        data={this.state.data}
                                    />
                                </div>

                                <div className="col-md-6">
                                    <Step1
                                        currentStep={this.state.currentStep}
                                        handleChange={this.handleChange}
                                        name={this.state.data.name}
                                        email={this.state.data.email}
                                        comment={this.state.data.comment}
                                        errors={this.state.errors}
                                    />

                                    <Step2
                                        currentStep={this.state.currentStep}
                                        handleChange={this.handleChange}
                                        clockTypes={this.state.clockTypes}
                                        clockType={this.state.data.clock_type}
                                        cities={this.state.cities}
                                        cityId={this.state.data.city_id}
                                        datetime={this.state.data.datetime}
                                        errors={this.state.errors}
                                    />

                                    <Step3
                                        currentStep={this.state.currentStep}
                                        handleChange={this.handleChange}
                                        masters={this.state.masters}
                                        masterId={this.state.data.master_id}
                                        errors={this.state.errors}
                                    />
                                </div>
                            </div>

                            {this.submitButton}
                            {this.nextButton}
                            {this.previousButton}

                        </form>
                    }

                </div>
            </section>
        )
    }
}
export default OrderForm;
