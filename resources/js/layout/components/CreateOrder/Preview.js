import React, {Component} from "react";

class Preview extends Component {
    get selectedClockText() {
        if (this.props.data.clock_type === null || this.props.data.clock_type === "")
            return null;

        let clockType = this.props.clockTypes.find((item) => {
            return item.name === this.props.data.clock_type;
        });

        return clockType !== null && clockType.title + ". " + clockType.description;
    }
    get selectedCityText() {
        if (this.props.data.city_id === null || this.props.data.city_id === "")
            return null;

        let city = this.props.cities.find(city => city.id.toString() === this.props.data.city_id);

        return city !== null ? city.name : "Не указан";
    }
    get selectedMasterText() {
        if (this.props.data.master_id === null || this.props.data.master_id === "")
            return null;

        let master = this.props.masters.find((item) => {
            return item.id.toString() === this.props.data.master_id;
        });

        return master !== null && master.name;
    }
    render() {
        return(
            <div>
                {/*Heading Text*/}
                <div className="section-heading">
                    <h2>Оформление заказа</h2>
                    <div className="line-shape"></div>
                </div>
                <div className="footer-text">
                    <p>Заполните все данные и мы подберем для Вас мастеров.</p>
                </div>
                { this.props.data.name &&
                <div className="address-text">
                    <p><span>Имя:</span> {this.props.data.name}</p>
                </div>
                }
                { this.props.data.email &&
                <div className="email-text">
                    <p><span>Email:</span> {this.props.data.email}</p>
                </div>
                }
                { this.props.data.comment &&
                <div className="address-text">
                    <p><span>Комментарий:</span> {this.props.data.comment}</p>
                </div>
                }
                { this.props.data.clock_type &&
                <div className="address-text">
                    <p><span>Тип часов:</span> {this.selectedClockText}</p>
                </div>
                }
                { this.props.data.city_id &&
                <div className="address-text">
                    <p><span>Город:</span> {this.selectedCityText}</p>
                </div>
                }
                { this.props.data.master_id &&
                <div className="address-text">
                    <p><span>Мастер:</span> {this.selectedMasterText}</p>
                </div>
                }
            </div>
        )
    }
}
export default Preview;
