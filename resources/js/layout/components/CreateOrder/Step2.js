import React, {Component} from "react";
import DatePicker from "react-datepicker";
import InputError from "./InputError";

class Step2 extends Component {
    _error(key) {
        let error = this.props.errors[key];

        return error ? error[0] : null;
    }
    constructor(props) {
        super(props);

        this.handleDateTimeChange = this.handleDateTimeChange.bind(this)
    }
    handleDateTimeChange(value) {
        this.props.handleChange({
            target: {
                name: "datetime",
                value: value
            }
        });
    }
    render() {
        if (this.props.currentStep !== 2) {
            return null
        }
        return(
            <div>
                <div className="row">
                    <div className="col-12">
                        <h3 className="step-heading">Шаг 2. Выбор города и типа часов</h3>
                        <div className="form-group">
                            <select
                                className="form-control"
                                id="clock_type"
                                name="clock_type"
                                onChange={this.props.handleChange}
                                value={this.props.clockType}
                            ><option value="" disabled={this.props.clockType !== null}>Выберите тип часов</option>
                                {this.props.clockTypes.map((type, i) =>
                                    <option key={i} value={type.name}>{type.title}</option>
                                )}</select>
                            <InputError error={this._error("clock_type")}/>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="form-group">
                            <select
                                className="form-control"
                                id="city_id"
                                name="city_id"
                                onChange={this.props.handleChange}
                                value={this.props.cityId}
                            ><option value="" disabled={this.props.cities !== null}>Выберите город</option>
                                {this.props.cities.map((city, i) =>
                                    <option key={i} value={city.id}>{city.name}</option>
                                )}</select>
                            <InputError error={this._error("city_id")}/>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="form-group">
                            <DatePicker
                                className="form-control"
                                onChange={this.handleDateTimeChange}
                                selected={this.props.datetime}
                                showTimeSelect
                                timeFormat="HH:mm"
                                timeIntervals={60}
                                minDate={new Date()}
                                dateFormat="dd-MM-yyyy HH:mm"
                            />
                            <InputError error={this._error("datetime")}/>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="form-group">
                            <InputError error={this._error("masters")}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Step2;
