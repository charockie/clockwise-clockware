import React, {Component} from "react";

class Success extends Component {
    render() {
        return(
            <div>
                <div className="row">
                    <div className="col-12">
                        <h2 className="step-heading">Спасибо за заказ.</h2>
                        <p className="text-success">Мы отправили Вам письмо с подтверждением, пожалуйста подтвердите Ваш заказ.</p>
                    </div>
                </div>
            </div>
        )
    }
}
export default Success;
