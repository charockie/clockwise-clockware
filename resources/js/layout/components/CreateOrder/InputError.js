import React, {Component} from "react";
import PropTypes from "prop-types";

class InputError extends Component {
    static propTypes = {
        error: PropTypes.string
    }
    render() {
        if (!this.props.error)
            return null;

        return(
            <div>
                <p><span className="text-danger">{this.props.error}</span></p>
            </div>
        )
    }
}
export default InputError;
