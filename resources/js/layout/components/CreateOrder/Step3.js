import React, {Component} from "react";
import InputError from "./InputError";

class Step3 extends Component {
    _error(key) {
        let error = this.props.errors[key];

        return error ? error[0] : null;
    }
    render() {
        if (this.props.currentStep !== 3) {
            return null
        }
        return(
            <div>
                <div className="row">
                    <div className="col-12">
                        <h3 className="step-heading">Шаг 3. Выбор мастера</h3>
                        <div className="form-group">
                            <select
                                className="form-control"
                                id="master_id"
                                name="master_id"
                                onChange={this.props.handleChange}
                                value={this.props.masterId}
                            ><option value="" disabled={this.props.masterId !== null}>Выберите мастера</option>
                                {this.props.masters.map((master, i) =>
                                    <option key={i} value={master.id}>{master.name} (рейтинг {master.rating})</option>
                                )}</select>
                            <InputError error={this._error("master_id")}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Step3;
