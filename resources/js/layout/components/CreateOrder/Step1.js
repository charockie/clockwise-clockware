import React, {Component} from "react";
import InputError from "./InputError";

class Step1 extends Component {
    _error(key) {
        let error = this.props.errors[key];

        return error ? error[0] : null;
    }
    render() {
        if (this.props.currentStep !== 1) {
            return null
        }
        return(
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="step-heading">Шаг 1. Контактные данные</h3>
                        <div className="contact_from">
                            <div className="contact_input_area">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <input
                                                className="form-control"
                                                id="name"
                                                name="name"
                                                type="text"
                                                placeholder="Ваше имя"
                                                value={this.props.name}
                                                onChange={this.props.handleChange}
                                            />
                                            <InputError error={this._error("name")}/>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <input
                                                className="form-control"
                                                id="email"
                                                name="email"
                                                type="text"
                                                placeholder="Ваш email"
                                                value={this.props.email}
                                                onChange={this.props.handleChange}
                                            />
                                            <InputError error={this._error("email")}/>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="form-group">
                                            <textarea
                                                className="form-control"
                                                id="comment"
                                                name="comment"
                                                placeholder="Комментарий"
                                                cols="30"
                                                rows="4"
                                                value={this.props.comment}
                                                onChange={this.props.handleChange}
                                            >{this.props.comment}</textarea>
                                            <InputError error={this._error("comment")}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Step1;
