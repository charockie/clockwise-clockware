import React, {Component} from "react";

class Features extends Component {
    render(){
        return (
            <section className="awesome-feature-area bg-white section_padding_100_70 clearfix" id="features">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/*Heading Text*/}
                            <div className="section-heading text-center">
                                <h2>Наши услуги</h2>
                                <div className="line-shape"></div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-user" aria-hidden="true"></i>
                                <h5>Диагностика</h5>
                                <p>Бесплатная диагностика Ваших часов.</p>
                            </div>
                        </div>
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-pulse" aria-hidden="true"></i>
                                <h5>Замена батарейки</h5>
                                <p>Заменим батарейку в Ваших часах и заведем их по новой.</p>
                            </div>
                        </div>
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-dashboard" aria-hidden="true"></i>
                                <h5>Выезд специалиста на дом</h5>
                                <p>Наш специалист приедет к Вам на дом и проведет базовую диагностику часов на
                                    месте.</p>
                            </div>
                        </div>
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-palette" aria-hidden="true"></i>
                                <h5>Доставка в сервисный центр</h5>
                                <p>Доставим Ваши часты в сервисный центр из указанного вами адреса.</p>
                            </div>
                        </div>
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-crown" aria-hidden="true"></i>
                                <h5>Профилактика</h5>
                                <p>Проведем профилактику, чистку и смажем Ваши часы.</p>
                            </div>
                        </div>
                        {/*Single Feature Start*/}
                        <div className="col-12 col-sm-6 col-lg-4">
                            <div className="single-feature">
                                <i className="ti-headphone" aria-hidden="true"></i>
                                <h5>24/7 Техническая поддержка</h5>
                                <p>Наши операторы работают круглосуточно для Вас и готовы ответить на все вопросы.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        )
    }
}
export default Features;
