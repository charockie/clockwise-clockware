import React, {Component} from "react";
import PropTypes from "prop-types";

class OurTeam extends Component {
    static propTypes = {
        team: PropTypes.arrayOf(
            PropTypes.shape({
                name: PropTypes.string.isRequired,
                rank: PropTypes.string.isRequired,
                image: PropTypes.string.isRequired,
                rating: PropTypes.number.isRequired
            })
        ).isRequired
    }
    render(){
        return (
            <section className="our-Team-area bg-white section_padding_100_70 clearfix" id="team">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            {/*Heading Text*/}
                            <div className="section-heading">
                                <h2>Наша команда</h2>
                                <div className="line-shape"></div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {this.props.team.map((profile, i) =>
                            <div key={i.toString()} className="col-12 col-md-6 col-lg-3">
                                <div className="single-team-member">
                                    <div className="member-image">
                                        <img src={profile.image} alt="" />
                                    </div>
                                    <div className="member-text">
                                        <h4>{profile.name}</h4>
                                        <p>{profile.rank}</p>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </section>
        )
    }
}
export default OurTeam;
