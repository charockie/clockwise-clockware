import React, {Component} from "react";

class Footer extends Component {
    render(){
        return (
            <footer className="footer-social-icon text-center section_padding_70 clearfix">
                <div className="footer-text">
                    <h2>Clockwise Clockware</h2>
                </div>
                <div className="footer-social-icon">
                    <a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i className="active fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#"> <i className="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
                </div>
                <div className="footer-menu">
                    <nav>
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Terms &amp; Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div className="copyright-text">
                    <p>Copyright ©2019. Clockwise Clockware</p>
                </div>
            </footer>
        )
    }
}
export default Footer;
