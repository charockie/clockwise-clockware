import React, {Component} from "react";

class Preloader extends Component {
    render(){
        if (this.props.loading === false)
            return null;

        return (
            <div className="container">
                {/*Preloader Start*/}
                <div id="preloader">
                    <div className="colorlib-load"></div>
                </div>
            </div>
        )
    }
}
export default Preloader;
