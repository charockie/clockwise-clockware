import React, {Component} from "react";

class Welcome extends Component {
    render(){
        return (
            <section className="wellcome_area clearfix" id="home">
                <div className="container h-100">
                    <div className="row h-100 align-items-center">
                        <div className="col-12 col-md">
                            <div className="wellcome-heading">
                                <h2>Clockwise Clockware</h2>
                                <h3>C</h3>
                                <p>Ваши часы в надежных руках</p>
                            </div>
                            <div className="get-start-area">
                                <a className="submit" href="#" onClick={this.props.scrollToOrderForm}>Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                </div>
                {/*Welcome thumb*/}
                <div className="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
                    <img srcSet="images/welcome-img.png" alt="" />
                </div>
            </section>
        )
    }
}
export default Welcome;
