import React, {Component} from "react";

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fixed: false,
            blockHeight: 110
        };
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }
    handleScroll = () => this.setState({fixed: window.scrollY > this.state.blockHeight});
    render(){
        return (
            <header className={this.state.fixed ? "header_area animated sticky slideInDown" : "header_area animated"}>
                <div className="container-fluid">
                    <div className="row align-items-center">
                        {/*Menu Area Start*/}
                        <div className="col-12 col-lg-10">
                            <div className="menu_area">
                                <nav className="navbar navbar-expand-lg navbar-light">
                                    {/*Logo*/}
                                    <a className="navbar-brand" href="#">Clockwise Clockware</a>
                                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                                            data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false"
                                            aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"></span>
                                    </button>
                                    {/*Menu Area*/}
                                    <div className="collapse navbar-collapse" id="ca-navbar">
                                        <ul className="navbar-nav ml-auto" id="nav">
                                            <li className="nav-item active">
                                                <a className="nav-link" href="#home">Главная</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#about">Почему мы?</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#features">Услуги</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#statistic">Статистика</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#pricing">Тарифы</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#team">Команда</a>
                                            </li>
                                        </ul>
                                        <div className="sing-up-button d-lg-none">
                                            <a href="#" onClick={this.props.scrollToOrderForm}>Оформить заказ</a>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        {/*Signup btn*/}
                        <div className="col-12 col-lg-2">
                            <div className="sing-up-button d-none d-lg-block">
                                <a href="#" onClick={this.props.scrollToOrderForm}>Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
export default Nav;
