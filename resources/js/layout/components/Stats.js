import React, {Component} from "react";
import PropTypes from "prop-types";

class Stats extends Component {
    static propTypes = {
        stats: PropTypes.shape({
            fixedCount: PropTypes.number.isRequired,
            mastersCount: PropTypes.number.isRequired,
            citiesCount: PropTypes.number.isRequired,
        })
    }
    render(){
        return (
            <section className="cool_facts_area section_padding_100_70 clearfix" id="statistic">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="header-area text-center">
                                <h2>Наша статистика</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {/*Single Stat Data*/}
                        <div className="col-12 col-md-4 col-lg-4">
                            <div className="single-cool-fact d-flex justify-content-center wow fadeInUp"
                                 data-wow-delay="0.2s">
                                <div className="counter-area">
                                    <h3><span className="counter">{ this.props.stats.fixedCount }</span></h3>
                                </div>
                                <div className="cool-facts-content">
                                    <i className="ion-clock"></i>
                                    <p>Отремонтировано <br/> часов</p>
                                </div>
                            </div>
                        </div>
                        {/*Single Stat Data*/}
                        <div className="col-12 col-md-4 col-lg-4">
                            <div className="single-cool-fact d-flex justify-content-center wow fadeInUp"
                                 data-wow-delay="0.4s">
                                <div className="counter-area">
                                    <h3><span className="counter">{ this.props.stats.mastersCount }</span></h3>
                                </div>
                                <div className="cool-facts-content">
                                    <i className="ion-person"></i>
                                    <p>Мастеров <br/>в работе</p>
                                </div>
                            </div>
                        </div>
                        {/*Single Stat Data*/}
                        <div className="col-12 col-md-4 col-lg-4">
                            <div className="single-cool-fact d-flex justify-content-center wow fadeInUp"
                                 data-wow-delay="0.8s">
                                <div className="counter-area">
                                    <h3><span className="counter">{ this.props.stats.citiesCount }</span></h3>
                                </div>
                                <div className="cool-facts-content">
                                    <i className="ion-planet"></i>
                                    <p>Работаем <br/> в городах</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Stats;
