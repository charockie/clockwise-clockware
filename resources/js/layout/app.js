/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require("./bootstrap");

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import React, {Component} from "react";
import ReactDOM, {render} from "react-dom";

import Preloader from "./components/Preloader";
import Nav from "./components/Nav";
import Welcome from "./components/Welcome";
import SpecialArea from "./components/SpecialArea";
import Features from "./components/Features";
import Stats from "./components/Stats";
import PricingPlane from "./components/PricingPlane";
import OurTeam from "./components/OurTeam";
import Footer from "./components/Footer";
import OrderForm from "./components/CreateOrder/OrderForm";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            appDataLoading: true,
            stats: {
                fixedCount: 0,
                mastersCount: 0,
                citiesCount: 0,
            },
            pricingPlans: [],
            ourTeam: [],
        };

        this.orderFormRef = React.createRef();

        this.loadData = this.loadData.bind(this);
        this.scrollToOrderForm = this.scrollToOrderForm.bind(this);
    }
    componentDidMount() {
        this.loadData();
    }
    loadData() {
        let stats = axios.get("/api/stats")
            .then((response) => { this.setState({stats: response.data}); return true; })
            .catch((error) => { console.error("API error", [error]); return false; });

        let pricingPlans = axios.get("/api/pricing-plans")
            .then((response) => { this.setState({pricingPlans: response.data}); return true; })
            .catch((error) => { console.error("API error", [error]); return false; });

        let ourTeam = axios.get("/api/masters")
            .then((response) => { this.setState({ourTeam: response.data.data}); return true; })
            .catch((error) => { console.error("API error", [error]); return false; });

        Promise.all([stats, pricingPlans, ourTeam]).then(values => {
            this.setState({appDataLoading: false})
        });
    }
    scrollToOrderForm = (event, data) => {
        event.preventDefault();

        let formElement = ReactDOM.findDOMNode(this.orderFormRef.current);
        window.scrollTo(0, formElement.offsetTop);

        if (data)
            this.orderFormRef.current.fillData(data);
    };
    render() {
        return (
            <div>
                <Preloader loading={this.state.appDataLoading}/>
                <Nav scrollToOrderForm={this.scrollToOrderForm}/>
                <Welcome scrollToOrderForm={this.scrollToOrderForm}/>
                <SpecialArea/>
                <Features/>
                <Stats stats={this.state.stats}/>
                <PricingPlane pricingPlans={this.state.pricingPlans} scrollToOrderForm={this.scrollToOrderForm}/>
                <OurTeam team={this.state.ourTeam}/>
                <OrderForm ref={this.orderFormRef}/>
                <Footer/>
            </div>
        )
    }
}

render(<App />, document.getElementById("app"));
