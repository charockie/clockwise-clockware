<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Rank;
use Faker\Generator as Faker;

$factory->define(Rank::class, function (Faker $faker) {
    return [
        "title" => $faker->word,
    ];
});
