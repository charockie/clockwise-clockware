<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\MasterBusyHour;
use App\Master;
use Faker\Generator as Faker;

$factory->define(MasterBusyHour::class, function (Faker $faker) {
    $dateFrom = \Carbon\Carbon::now()
        ->setMinute(0)
        ->setSecond(0)
        ->setMicrosecond(0)
        ->addYear();

    $dateTo = clone $dateFrom;
    $dateTo->addHours(rand(1, 3));

    return [
        "master_id" => factory(Master::class)->create()->id,
        "started_at" => $dateFrom,
        "ended_at" => $dateTo,
    ];
});
