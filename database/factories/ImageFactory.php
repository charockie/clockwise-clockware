<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use \App\Image;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Image::class, function (Faker $faker) {
    $diskName = "public";
    $pathPrefix = "masters-images";
    $disk = Storage::disk($diskName);
    $disk->createDir($pathPrefix);

    $imageFileName = $faker->image(storage_path("framework/testing/disks/public/{$pathPrefix}"), 450, 300, null, false);
    return [
        "name" => explode(".", $imageFileName)[0],
        "path_prefix" => $pathPrefix,
        "disk" => $diskName,
        "mime" => "jpg",
    ];
}, "master-image");
