<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Master;
use Faker\Generator as Faker;

$factory->define(Master::class, function (Faker $faker) {
    return [
        "name" => $faker->firstName,
        "surname" => $faker->lastName,
        "rating" => rand(10, 50) / 10,
    ];
});
