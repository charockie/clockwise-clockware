<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Customer;
use App\City;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        "name" => $faker->name,
        "email" => $faker->email,
        "city_id" => factory(City::class)->create()->id
    ];
});
