<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use App\Customer;
use App\Master;
use App\MasterBusyHour;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        "comment" => $faker->text(),
        "customer_id" => factory(Customer::class)->create()->id,
        "master_id" => factory(Master::class)->create()->id,
        "master_busy_hours_id" => factory(MasterBusyHour::class)->create()->id,
        "is_confirmed" => false,
    ];
});

$factory->define(Order::class, function (Faker $faker) {
    return [
        "comment" => $faker->text(),
        "customer_id" => factory(Customer::class)->create()->id,
        "master_id" => factory(Master::class)->create()->id,
        "master_busy_hours_id" => factory(MasterBusyHour::class)->create()->id,
        "is_confirmed" => true,
    ];
}, "order-confirmed");
