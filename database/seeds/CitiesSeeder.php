<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            ["name" => "Днепр"],
            ["name" => "Ужгород"],
        ];

        if (!\App\City::query()->exists()) {
            foreach ($cities as $city) {
                \App\City::firstOrCreate($city);
            }
        }
    }
}
