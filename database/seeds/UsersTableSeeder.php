<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect([
            [
                "name" => "admin",
                "email" => "admin@example.com",
                "password" => bcrypt("passwordsecret"),
            ]
        ]);

        $users->each(function (array $userData) {
            \App\User::query()->updateOrCreate(["email" => $userData["email"]], $userData);
        });
    }
}
