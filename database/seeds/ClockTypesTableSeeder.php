<?php

use Illuminate\Database\Seeder;

class ClockTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                "name" => "small_clock",
                "title" => "Маленькие часы",
                "description" => "Ремонт занимает 1 час",
                "duration" => 1,
            ],
            [
                "name" => "middle_clock",
                "title" => "Средние часы",
                "description" => "Ремонт занимает 2 часа",
                "duration" => 2,
            ],
            [
                "name" => "big_clock",
                "title" => "Большие часы",
                "description" => "Ремонт занимает 3 часа",
                "duration" => 3,
            ],
        ];

        if (!\App\ClockType::query()->exists()) {
            foreach ($cities as $city) {
                \App\ClockType::firstOrCreate($city);
            }
        }
    }
}
