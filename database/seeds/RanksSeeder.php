<?php

use Illuminate\Database\Seeder;

class RanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            ["title" => "Мастер"],
            ["title" => "Специалист"],
            ["title" => "Стажер"],
        ];

        if (!\App\Rank::query()->exists()) {
            foreach ($ranks as $rank) {
                \App\Rank::firstOrCreate($rank);
            }
        }
    }
}
