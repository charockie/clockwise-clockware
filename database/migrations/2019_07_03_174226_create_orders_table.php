<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("comment")->nullable();

            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')->on('customers')->references('id')->onDelete('cascade');

            $table->integer('master_id')->unsigned();
            $table->foreign('master_id')->on('masters')->references('id')->onDelete('cascade');

            $table->bigInteger('master_busy_hours_id')->unsigned();
            $table->foreign('master_busy_hours_id')->on('masters_busy_hours')->references('id')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
