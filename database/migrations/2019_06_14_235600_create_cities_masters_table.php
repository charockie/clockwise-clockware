<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_masters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->on('cities')->references('id')->onDelete('cascade');

            $table->integer('master_id')->unsigned();
            $table->foreign('master_id')->on('masters')->references('id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_masters');
    }
}
