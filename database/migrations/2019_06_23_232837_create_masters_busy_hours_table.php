<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersBusyHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters_busy_hours', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('master_id')->unsigned();
            $table->foreign('master_id')->on('masters')->references('id')->onDelete('cascade');

            $table->timestamp("started_at")->nullable();
            $table->timestamp("ended_at")->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters_busy_hours');
    }
}
