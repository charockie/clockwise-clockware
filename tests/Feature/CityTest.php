<?php

namespace Tests\Feature;

use App\City;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show city
     */
    public function test_can_show_cities_list() {
        factory(City::class)->create();

        $this->get(route("api.landing.cities"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "name",
                    ]
                ],
            ]);
    }
}
