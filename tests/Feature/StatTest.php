<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show stats
     */
    public function test_can_show_stats() {
        $this->get(route("api.landing.stats"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "mastersCount",
                "fixedCount",
                "citiesCount",
            ]);
    }
}
