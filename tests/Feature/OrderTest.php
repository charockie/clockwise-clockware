<?php

namespace Tests\Feature;

use App\City;
use App\ClockType;
use App\Master;
use App\Order;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can path step one on make order
     */
    public function test_can_path_step_one_on_make_order()
    {
        $data = [
            "step" => 1,
            "email" => $this->faker->email,
            "name" => $this->faker->name,
            "comment" => $this->faker->text(),
        ];

        $this->post('/', $data)
            ->assertStatus(200)
            ->assertJson(["validate" => true]);
    }

    /**
     * Test can path step two on make order
     */
    public function test_can_path_step_two_on_make_order()
    {
        $clockType = ClockType::query()->inRandomOrder()->first();
        $city = factory(City::class)->create();

        $data = [
            "step" => 2,
            "email" => $this->faker->email,
            "name" => $this->faker->name,
            "comment" => $this->faker->text(),
            "clock_type" => $clockType->name,
            "city_id" => $city->id,
            "datetime" => $this->faker->dateTimeBetween("now", "+1 year")
                ->setTime(rand(0,24), 0,0,0)
                ->format("m/d/Y, H:i:s"),
        ];

        $this->post('/', $data)
            ->assertStatus(200)
            ->assertJson(["validate" => true]);
    }

    /**
     * Test can path step two on make order
     */
    public function test_master_is_busy_on_make_order()
    {
        $datetime = $this->faker->dateTimeBetween("now", "+1 year")
            ->setTime(rand(0,24), 0,0,0);
        $clockType = ClockType::query()->inRandomOrder()->first();
        $city = factory(City::class)->create();
        $master = factory(Master::class)->create();
        /** @var Master $master */
        $master->cities()->sync([$city->id]);

        $busyFrom = Carbon::parse($datetime);
        $busyTo = (clone $busyFrom)->addHours($clockType->duration);
        $master->busyHours()->create([
            "started_at" => $busyFrom,
            "ended_at" => $busyTo,
        ]);

        $data = [
            "email" => $this->faker->email,
            "name" => $this->faker->name,
            "comment" => $this->faker->text(),
            "clock_type" => $clockType->name,
            "city_id" => $city->id,
            "datetime" => $datetime->format("m/d/Y, H:i:s"),
            "master_id" => $master->id,
        ];

        $response = $this->postJson('/', $data);
        $response->assertStatus(422)
            ->assertJsonValidationErrors(["master_id" => "Выбранный мастер занят в выбранное время"]);
    }

    /**
     * Test can make order
     */
    public function test_can_make_order()
    {
        $clockType = ClockType::query()->inRandomOrder()->first();
        $city = factory(City::class)->create();
        $master = factory(Master::class)->create();
        $master->cities()->sync([$city->id]);

        $data = [
            "email" => $this->faker->email,
            "name" => $this->faker->name,
            "comment" => $this->faker->text(),
            "clock_type" => $clockType->name,
            "city_id" => $city->id,
            "datetime" => $this->faker->dateTimeBetween("now", "+1 year")
                ->setTime(rand(0,24), 0,0,0)
                ->format("m/d/Y, H:i:s"),
            "master_id" => $master->id,
        ];

        $this->post('/', $data)
            ->assertStatus(200)
            ->assertJson(["success" => true]);
    }

    /**
     * Test can confirm order
     *
     * @return void
     */
    public function test_can_confirm_order()
    {
        $order = factory(Order::class)->create();

        $this->get(route("order.confirm", ["token" => encrypt($order->id)]))
            ->assertStatus(302)
            ->assertRedirect('/');

        $updatedOrder = Order::query()->find($order->id);
        $this->assertTrue($updatedOrder->is_confirmed);
    }

    /**
     * Test can`t confirm confirmed order
     *
     * @return void
     */
    public function test_can_not_confirm_confirmed_order()
    {
        $order = factory(Order::class, "order-confirmed")->create();

        $this->get(route("order.confirm", ["token" => encrypt($order->id)]))
            ->assertStatus(404);
    }
}
