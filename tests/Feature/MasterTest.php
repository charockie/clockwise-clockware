<?php

namespace Tests\Feature;

use App\City;
use App\ClockType;
use App\Master;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MasterTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show masters
     */
    public function test_can_show_masters_list()
    {
        factory(Master::class, 2)->create();

        $this->get(route("api.landing.masters"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "name",
                        "rank",
                        "rating",
                        "image",
                    ]
                ],
            ]);
    }

    /**
     * Test can show free masters at the moment
     *
     * @dataProvider busyMasterTimesProvider
     * @param string $moment
     * @param string $busyFrom
     * @param string $clockType
     * @param int $mastersCount
     */
    public function test_can_show_free_masters_list_at_moment(string $moment, string $busyFrom, string $clockType, int $mastersCount)
    {
        $moment = Carbon::parse($moment);
        $busyFrom = Carbon::parse($busyFrom);

        $clockType = ClockType::query()->where("name", $clockType)->first();
        $city = factory(City::class)->create();
        $masters = factory(Master::class, $mastersCount)->create()->each(function (Master $master) use ($city) {
            $master->cities()->sync([$city->id]);
        });

        $busyTo = (clone $busyFrom)->addHours($clockType->duration);

        $master = $masters->first();
        $busyHours = $master->busyHours()->create([
            "started_at" => $busyFrom,
            "ended_at" => $busyTo,
        ]);

        $response = $this->get(route("api.landing.masters", [
            "dateFrom" => $moment->format("m/d/Y, H:i:s"),
            "clock_type" => $clockType->name,
            "city" => $city->id,
        ]));

        $this->assertEquals($busyHours->master->id, $master->id);
        $this->assertCount($mastersCount-1, $response->decodeResponseJson("data"));
    }

    /**
     * @return array
     */
    public function busyMasterTimesProvider()
    {
        return [
            ["2020/06/25 15:00", "2020/06/25 15:00", "small_clock",     5],

            ["2020/06/25 15:00", "2020/06/25 14:00", "middle_clock",    5],
            ["2020/06/25 15:00", "2020/06/25 15:00", "middle_clock",    5],
            ["2020/06/25 15:00", "2020/06/25 16:00", "middle_clock",    5],

            ["2020/06/25 15:00", "2020/06/25 13:00", "big_clock",       5],
            ["2020/06/25 15:00", "2020/06/25 14:00", "big_clock",       5],
            ["2020/06/25 15:00", "2020/06/25 15:00", "big_clock",       5],
            ["2020/06/25 15:00", "2020/06/25 16:00", "big_clock",       5],
            ["2020/06/25 15:00", "2020/06/25 17:00", "big_clock",       5],
        ];
    }
}
