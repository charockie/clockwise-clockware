<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlanTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show pricing planes
     */
    public function test_can_show_pricing_plans_list() {
        $this->get(route("api.landing.pricing-plans"))
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    "name",
                    "title",
                    "cost",
                    "duration",
                    "descriptions"
                ]
            ]);
    }
}
