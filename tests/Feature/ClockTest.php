<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClockTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show clocks
     */
    public function test_can_show_clocks_list() {
        $this->get(route("api.landing.clocks"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "name",
                        "title",
                        "description",
                        "duration",
                    ]
                ],
            ]);
    }
}
