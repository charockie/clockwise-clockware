<?php

namespace Tests\Unit\Middleware;

use App\Http\Middleware\RedirectIfAuthenticated;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RedirectIfAuthenticatedTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can path step one on make order
     */
    public function test_redirect_authenticated_user()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $request = new Request();
        $middleware = new RedirectIfAuthenticated();

        $response = $middleware->handle($request, function () {});

        $this->assertEquals($response->getStatusCode(), 302);
    }

    /**
     * Test can path step one on make order
     */
    public function test_handle_not_authenticated_user()
    {
        $request = new Request();
        $middleware = new RedirectIfAuthenticated();

        $response = $middleware->handle($request, function () {});

        $this->assertEquals($response, null);
    }
}
