<?php

namespace Tests\Unit\Models;

use App\Master;
use App\Rank;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RankTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show rank masters
     */
    public function test_can_show_masters_list() {
        /** @var Rank $rank */
        $rank = factory(Rank::class)->create();
        $masters = factory(Master::class, 5)->create()->each(function (Master $master) use ($rank) {
            $master->rank()->associate($rank);
            $master->save();
        });

        $this->assertCount($masters->count(), $rank->masters);
    }
}
