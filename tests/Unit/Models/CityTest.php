<?php

namespace Tests\Unit\Models;

use App\City;
use App\Master;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show city masters
     */
    public function test_can_show_masters_list() {
        /** @var City $city */
        $city = factory(City::class)->create();
        $masters = factory(Master::class, 3)->create()->each(function (Master $master) use ($city) {
            $master->cities()->sync([$city->id]);
        });

        $this->assertCount($masters->count(), $city->masters);
    }
}
