<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MakeOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $step1Rules = [
            "email" => [
                "required",
                "email",
                "max:50",
            ],
            "name" => [
                "required",
                "min:3",
                "max:50",
            ],
            "comment" => [
                "sometimes",
                "nullable",
                "min:5",
                "max:255",
            ],
        ];

        $step2Rules = [
            "clock_type" => [
                "required",
                "string",
                "exists:clock_types,name",
            ],
            "city_id" => [
                "required",
                "numeric",
                 "exists:cities,id",
            ],
            "datetime" => [
                "required",
                "date",
                "after_or_equal:". Carbon::now()->addHour()->format("m/d/Y, H:i:s")
            ],
        ];

        $step3Rules = [
            "master_id" => [
                "required",
                "numeric",
                Rule::exists("masters", "id")
            ],
        ];

        switch ($this->get("step")) {
            case 1:
                $rules = $step1Rules;
                break;
            case 2:
                $rules = array_merge($step1Rules, $step2Rules);
                break;
            default:
                $rules = array_merge($step1Rules, $step2Rules, $step3Rules);
                break;
        }

        return $rules;
    }
}
