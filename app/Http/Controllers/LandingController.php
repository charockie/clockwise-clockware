<?php

namespace App\Http\Controllers;

use App\ClockType;
use App\Customer;
use App\Http\Requests\ConfirmOrderRequest;
use App\Http\Requests\MakeOrderRequest;
use App\Master;
use App\Notifications\Customer\OrderCreatedNotification;
use App\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class LandingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('landing');
    }
}
