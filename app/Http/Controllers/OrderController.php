<?php

namespace App\Http\Controllers;

use App\ClockType;
use App\Customer;
use App\Http\Requests\ConfirmOrderRequest;
use App\Http\Requests\MakeOrderRequest;
use App\Master;
use App\Notifications\Customer\OrderCreatedNotification;
use App\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class OrderController extends Controller
{
    /**
     * @param MakeOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function make(MakeOrderRequest $request)
    {
        if ($request->has("step") && $request->input("step") < 3)
            return response()->json(["validate" => true]);

        $clockType = ClockType::query()
            ->where("name", $request->input("clock_type"))
            ->first();

        $dateFrom = Carbon::parse($request->input("datetime"));
        $dateTo = (clone $dateFrom)->addHours($clockType->duration);

        /** @var Master $master */
        $master = Master::query()
            ->whereHas("cities", function (Builder $query) use ($request) {
                $query->where("cities.id", $request->get("city_id"));
            })
            ->whereHas("busyHours", function (Builder $query) use ($dateFrom, $dateTo) {
                $query->busyBetweenDatetime($dateFrom, $dateTo);
            }, "<")
            ->where("id", $request->input("master_id"))
            ->first();

        if (!$master) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                "master_id" => ["Выбранный мастер занят в выбранное время"],
            ]);
            throw $error;
        }

        /** @var Customer $customer */
        $customer = Customer::query()->firstOrNew(["email" => $request->input("email")]);
        $customer->fill($request->all());
        $customer->city()->associate($request->input("city_id"));
        $customer->save();

        $masterBusyHours = $master->busyHours()->create([
            "started_at" => $dateFrom,
            "ended_at" => $dateTo,
        ]);

        /** @var Order $order */
        $order = Order::make($request->all());
        $order->customer()->associate($customer);
        $order->master()->associate($master);
        $order->masterBusyHours()->associate($masterBusyHours);
        $status = $order->save();

        if ($status)
            $customer->notify(new OrderCreatedNotification($order));

        return response()->json(["success" => $status]);
    }

    /**
     * @param ConfirmOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function confirm(ConfirmOrderRequest $request)
    {
        $orderId = decrypt($request->input("token"));

        $order = Order::query()
            ->where("is_confirmed", false)
            ->findOrFail($orderId);

        $order->setAttribute("is_confirmed", true);
        $order->save();

        return redirect('/');
    }
}
