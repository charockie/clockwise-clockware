<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class PlanController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = new Collection([
            [
                "name" => "small_clock",
                "title" => "Маленькие часы",
                "cost" => 100,
                "duration" => 1,
                "descriptions" => [
                    "Ремонт занимает 1 час",
                    "Очистка от коррозии",
                    "Смазка механизма",
                    "Замена батарейки",
                ]
            ],
            [
                "name" => "middle_clock",
                "title" => "Средние часы",
                "cost" => 150,
                "duration" => 2,
                "descriptions" => [
                    "Ремонт занимает 2 часа",
                    "Очистка от коррозии",
                    "Смазка механизма",
                    "Замена батарейки",
                ]
            ],
            [
                "name" => "big_clock",
                "title" => "Большие часы",
                "cost" => 200,
                "duration" => 3,
                "descriptions" => [
                    "Ремонт занимает 3 час",
                    "Очистка от коррозии",
                    "Смазка механизма",
                    "Замена батарейки",
                ]
            ],
        ]);

        return response()->json($data);
    }
}
