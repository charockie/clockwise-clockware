<?php

namespace App\Http\Controllers\Api;

use App\ClockType;
use App\Http\Resources\ClockResource;
use App\Http\Controllers\Controller;

class ClockController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $clockTypes = ClockType::all();

        return ClockResource::collection($clockTypes);
    }
}
