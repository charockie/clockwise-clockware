<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class StatController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = new Collection([
            "mastersCount"  => rand(8,10),
            "fixedCount"    => rand(90,120),
            "citiesCount"   => rand(3, 5),
        ]);

        return response()->json($data);
    }
}
