<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Resources\CityResource;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $cities = City::all();

        return CityResource::collection($cities);
    }
}
