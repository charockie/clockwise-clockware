<?php

namespace App\Http\Controllers\Api;

use App\ClockType;
use App\Http\Resources\MasterResource;
use App\Master;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if ($request->has(["dateFrom", "clock_type", "city"])) {
            $clockType = ClockType::query()
                ->where("name", $request->input("clock_type"))
                ->firstOrFail();

            $dateFrom = Carbon::parse($request->input("dateFrom"));
            $dateTo = (clone $dateFrom)->addHours($clockType->duration);

            $masters = Master::query()
                ->whereHas("cities", function (Builder $query) use ($request) {
                    $query->where("cities.id", $request->get("city"));
                })
                ->whereHas("busyHours", function (Builder $query) use ($dateFrom, $dateTo) {
                    $query->busyBetweenDatetime($dateFrom, $dateTo);
                }, "<")
                ->get();
        } else
            $masters = Master::query()->inRandomOrder()->limit(4)->get();

        return MasterResource::collection($masters);
    }
}
