<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MasterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => "{$this->name} {$this->surname}",
            "rank" => $this->rank ? $this->rank->title : "",
            "rating" => $this->rating,
            "image" => $this->image ? $this->image->url : null,
        ];
    }
}
