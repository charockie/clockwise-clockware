<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "images";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "path_prefix",
        "disk",
        "mime",
    ];

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return Storage::disk($this->disk)->url("{$this->path_prefix}/{$this->name}");
    }
}
