<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterBusyHour extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "masters_busy_hours";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "started_at",
        "ended_at",
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        "started_at",
        "ended_at",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function master()
    {
        return $this->belongsTo(Master::class, "master_id");
    }

    /**
     * @param Builder $query
     * @param Carbon $from
     * @param Carbon $to
     * @return Builder
     */
    public function scopeBusyBetweenDatetime(Builder $query, Carbon $from, Carbon $to) : Builder
    {
        $query->where(function (Builder $query) use (&$from, &$to) {
            $query
                ->where("started_at", ">=", $from)
                ->where("started_at", "<", $to);
        })->orWhere(function (Builder $query) use (&$from, &$to) {
            $query
                ->where("ended_at", ">", $from)
                ->where("ended_at", "<=", $to);
        });

        return $query;
    }

    /**
     * @param Carbon $value
     * @return $this
     */
    public function setStartedAtAttribute(Carbon $value)
    {
        $value
            ->setMinutes(0)
            ->setSeconds(0)
            ->setMicroseconds(0);

        $this->attributes["started_at"] = $value;

        return $this;
    }

    /**
     * @param Carbon $value
     * @return $this
     */
    public function setEndedAtAttribute(Carbon $value)
    {
        $value
            ->setMinutes(0)
            ->setSeconds(0)
            ->setMicroseconds(0);

        $this->attributes["ended_at"] = $value;

        return $this;
    }
}
