<?php

namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     * @codeCoverageIgnore
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @codeCoverageIgnore
     */
    public function boot()
    {
        if (env('HTTPS_SCHEME') === true) {
            URL::forceScheme('https');
        }
    }
}
