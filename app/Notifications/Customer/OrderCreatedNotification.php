<?php

namespace App\Notifications\Customer;

use App\Customer;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderCreatedNotification extends Notification
{
    use Queueable;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification"s delivery channels.
     *
     * @param  Customer $notifiable
     * @return array
     */
    public function via(Customer $notifiable)
    {
        return ["mail"];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Customer $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(Customer $notifiable)
    {
        return (new MailMessage)
            ->subject("Заказ №{$this->order->id}")
            ->greeting("Заказ успешно оформлен")
            ->line("ID заказа: {$this->order->id}")
            ->line("Мастер: {$this->order->master->surname} {$this->order->master->name}")
            ->line("Ремонт заказан на время: {$this->order->masterBusyHours->started_at->format("d.m.Y H:i")} - {$this->order->masterBusyHours->ended_at->format("d.m.Y H:i")}")
            ->action("Подтвердите заказ", route("order.confirm", ["token" => encrypt($this->order->id)]))
            ->line("Спасибо Вам за обрщение к нам!");
    }
}
