<?php

namespace Modules\Admin\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MasterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "surname" => $this->surname,
            "rating" => $this->rating,
            "rank" => $this->rank ? $this->rank->title : null,
            "rank_id" => $this->rank ? $this->rank_id : null,
            "image_id" => $this->image ? $this->image->id : null,
            "image_url" => $this->image ? $this->image->url : null,
            "cities" => $this->cities ? $this->cities->pluck("id") : [],
            "cities_names" => $this->cities ? $this->cities->pluck("name") : [],
            "created_at" => $this->created_at->format($this->getDateFormat()),
            "updated_at" => $this->updated_at->format($this->getDateFormat()),
        ];
    }
}
