<?php

namespace Modules\Admin\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "city" => CityResource::make($this->city),
            "created_at" => $this->created_at->format($this->getDateFormat()),
            "updated_at" => $this->updated_at->format($this->getDateFormat()),
        ];
    }
}
