<?php

namespace Modules\Admin\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "comment" => $this->comment,
            "is_confirmed" => $this->is_confirmed,
            "customer" => CustomerResource::make($this->customer),
            "master" => MasterResource::make($this->master),
            "master_busy_hours" => [
                "started_at" => $this->masterBusyHours->started_at->format($this->masterBusyHours->getDateFormat()),
                "ended_at" => $this->masterBusyHours->ended_at->format($this->masterBusyHours->getDateFormat()),
            ],
            "created_at" => $this->created_at->format($this->getDateFormat()),
            "updated_at" => $this->updated_at->format($this->getDateFormat()),
        ];
    }
}
