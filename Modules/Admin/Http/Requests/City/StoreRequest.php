<?php

namespace Modules\Admin\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => [
                "required",
                Rule::unique("cities")->whereNull("deleted_at"),
                "string",
                "regex:/^[\pL\s\-]+$/u",
                "min:3",
                "max:40",
            ]
        ];
    }
}
