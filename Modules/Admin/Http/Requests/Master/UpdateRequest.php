<?php

namespace Modules\Admin\Http\Requests\Master;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => [
                "required",
                "string",
                "regex:/^[\pL\s\-]+$/u",
                "min:3",
                "max:40",
            ],
            "surname" => [
                "required",
                "string",
                "regex:/^[\pL\s\-]+$/u",
                "min:3",
                "max:40",
            ],
            "rating" => [
                "required",
                "numeric",
                "min:0",
                "max:5",
            ],
            "cities" => [
                "nullable",
                "array",
            ],
            "cities.*" => [
                "required",
                "numeric",
                "exists:cities,id",
            ],
            "rank_id" => [
                "present",
                "nullable",
                "numeric",
                "exists:ranks,id"
            ],
            "image_id" => [
                "present",
                "nullable",
                "numeric",
                "exists:images,id"
            ],
        ];
    }
}
