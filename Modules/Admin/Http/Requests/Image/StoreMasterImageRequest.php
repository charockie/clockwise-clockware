<?php

namespace Modules\Admin\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;

class StoreMasterImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "file" => [
                "required",
                "image",
                "dimensions:ratio=3/2,min_width=255",
                "max: 5120", // 5MB
            ],
        ];
    }
}
