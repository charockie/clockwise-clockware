<?php

namespace Modules\Admin\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route("customer");

        return [
            "email" => [
                "required",
                "email",
                "max:50",
                Rule::unique('customers')->ignore($id)->whereNull("deleted_at"),
            ],
            "name" => [
                "required",
                "min:3",
                "max:50",
            ],
            "city_id" => [
                "required",
                "numeric",
                "exists:cities,id"
            ],
        ];
    }
}
