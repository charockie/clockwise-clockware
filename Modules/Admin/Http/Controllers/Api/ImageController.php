<?php

namespace Modules\Admin\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Admin\Http\Requests\Image\StoreMasterImageRequest;
use Modules\Admin\Resources\ImageResource;
use Modules\Admin\Resources\MasterResource;
use App\Image;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMasterImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMasterImage(StoreMasterImageRequest $request)
    {
        $diskName = "public";
        $pathPrefix = "masters-images";
        $disk = Storage::disk($diskName);
        $disk->createDir($pathPrefix);

        $file = $request->file("file");
        $img = \Intervention\Image\Facades\Image::make($file);

        $filePathForStoring = $file->hashName($disk->path($pathPrefix));

        $ratio = 3/2;
        $width = 255;
        $height = round(255 / $ratio);
        $img->resize($width, $height)
            ->save($filePathForStoring);

        $image = Image::create([
            "name" => $file->hashName(),
            "path_prefix" => $pathPrefix,
            "disk" => $diskName,
            "mime" => $file->getMimeType(),
        ]);

        return response()->json(["data" => new ImageResource($image)], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Image $image
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Image $image)
    {
        $image->delete();

        return response(null, 204);
    }
}
