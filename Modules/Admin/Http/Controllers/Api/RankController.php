<?php

namespace Modules\Admin\Http\Controllers\Api;

use App\City;
use Modules\Admin\Http\Requests\City\StoreRequest;
use Modules\Admin\Http\Requests\City\UpdateRequest;
use Modules\Admin\Resources\CityResource;
use App\Rank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RankController extends Controller
{
    /**
     * Display a short listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function shortList()
    {
        return response()->json(["data" => Rank::query()->pluck("title", "id")]);
    }
}
