<?php

namespace Modules\Admin\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Admin\Http\Requests\Master\StoreRequest;
use Modules\Admin\Http\Requests\Master\UpdateRequest;
use Modules\Admin\Resources\MasterResource;
use App\Master;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return MasterResource::collection(Master::paginate($request->input("per_page", 15)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $master = Master::create($request->all());
        $master->cities()->sync($request->input("cities", []));

        return response()->json(["data" => new MasterResource($master)], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Master $master
     * @return MasterResource
     */
    public function show(Master $master)
    {
        return new MasterResource($master);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Master $master
     * @return MasterResource
     */
    public function update(UpdateRequest $request, Master $master)
    {
        $master->update($request->all());
        $master->cities()->sync($request->input("cities", []));

        return new MasterResource($master);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Master $master
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Master $master)
    {
        $master->delete();

        return response(null, 204);
    }
}
