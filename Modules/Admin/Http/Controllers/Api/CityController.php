<?php

namespace Modules\Admin\Http\Controllers\Api;

use App\City;
use Modules\Admin\Http\Requests\City\StoreRequest;
use Modules\Admin\Http\Requests\City\UpdateRequest;
use Modules\Admin\Resources\CityResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return CityResource::collection(City::paginate($request->input("per_page", 15)));
    }

    /**
     * Display a short listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function shortList()
    {
        return response()->json(["data" => City::query()->pluck("name", "id")]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return CityResource
     */
    public function store(StoreRequest $request)
    {
        $city = City::create($request->all());

        return response()->json(["data" => new CityResource($city)], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param City $city
     * @return CityResource
     */
    public function show(City $city)
    {
        return new CityResource($city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param City $city
     * @return CityResource
     */
    public function update(UpdateRequest $request, City $city)
    {
        $city->update($request->all());

        return new CityResource($city);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param City $city
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(City $city)
    {
        $city->delete();

        return response(null, 204);
    }
}
