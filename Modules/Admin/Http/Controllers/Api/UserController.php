<?php

namespace Modules\Admin\Http\Controllers\Api;

use Modules\Admin\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return UserResource
     */
    public function showCurrent(Request $request)
    {
        return new UserResource($request->user());
    }
}
