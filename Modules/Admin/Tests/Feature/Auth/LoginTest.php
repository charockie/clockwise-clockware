<?php

namespace Modules\Admin\Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test user can login with correct credentials
     */
    public function test_user_can_login_with_correct_credentials()
    {
        $user = factory(User::class)->create(["password" => bcrypt($password = "password")]);

        $response = $this->post(route("admin.login"), [
            "email" => $user->email,
            "password" => $password,
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "user",
                "api_key",
            ]);
        $this->assertAuthenticatedAs($user);
    }

    /**
     * Test user can`t login with correct credentials
     */
    public function test_user_cannot_login_with_incorrect_credentials()
    {
        $user = factory(User::class)->create(["password" => bcrypt($password = "password")]);

        $response = $this->postJson(route("admin.login"), [
            "email" => $user->email,
            "password" => "password1",
        ]);

        $response->assertStatus(422);
        $this->assertFalse($this->isAuthenticated());
    }

    /**
     * Test user can`t login with correct credentials
     */
    public function test_user_can_success_logout()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->post(route("admin.logout"));

        $response->assertStatus(302);
        $this->assertFalse($this->isAuthenticated());
    }
}
