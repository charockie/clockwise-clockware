<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\Customer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show customer
     */
    public function test_can_show_customers_list_with_pagination() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(Customer::class)->create();

        $this->get(route("api.admin.customers.index"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "name",
                        "email",
                        "city",
                        "created_at",
                        "updated_at",
                    ]
                ],
                "links" => [
                    "first",
                    "last",
                    "prev",
                    "next",
                ],
                "meta" => [
                    "current_page",
                    "from",
                    "last_page",
                    "path",
                    "per_page",
                    "to",
                    "total",
                ],
            ]);
    }

    /**
     * Test can show customer
     */
    public function test_can_show_customer() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $customer = factory(Customer::class)->create();

        $this->get(route("api.admin.customers.show", $customer->id))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "name",
                    "email",
                    "city",
                    "created_at",
                    "updated_at",
                ],
            ]);
    }

    /**
     * Test can update customer
     */
    public function test_can_update_customer() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $customer = factory(Customer::class)->create();

        $data = [
            "name" => $this->faker->name,
            "email" => $this->faker->email,
            "city_id" => $customer->city->id
        ];

        $this->put(route("api.admin.customers.update", $customer->id), $data)
            ->assertStatus(200)
            ->assertJson(["data" => [
                "name" => $data["name"],
                "email" => $data["email"],
                "city" => [
                    "id" => $customer->city->id,
                    "name" => $customer->city->name,
                ]
            ]]);
    }

    /**
     * Test can destroy customer
     */
    public function test_can_destroy_customer() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $customer = factory(Customer::class)->create();

        $this->delete(route("api.admin.customers.destroy", $customer->id))
            ->assertStatus(204);
    }
}
