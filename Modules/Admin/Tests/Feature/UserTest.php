<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show Ranks short list
     */
    public function test_can_show_current_user() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $this->get(route("api.admin.users.current"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "name",
                    "email",
                    "email_verified_at",
                    "created_at",
                    "updated_at",
                ]
            ]);
    }
}
