<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\Image;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImageTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show Ranks short list
     */
    public function test_can_store_master_image() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        Storage::fake('public');

        $this->post(route("api.admin.images.store.master-image"), [
            'file' => UploadedFile::fake()->image('image.jpg', 450, 300)
        ])->assertStatus(201)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "url",
                    "created_at",
                    "updated_at",
                ]
            ]);
    }

    /**
     * Test can show Ranks short list
     */
    public function test_can_delete_image() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        Storage::fake('public');

        $image = factory(Image::class, "master-image")->create();

        $this->delete(route("api.admin.images.destroy", [$image]))
            ->assertStatus(204);
    }
}
