<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\City;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show city
     */
    public function test_can_show_cities_list_with_pagination() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(City::class)->create();

        $this->get(route("api.admin.cities.index"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "name",
                        "created_at",
                        "updated_at",
                    ]
                ],
                "links" => [
                    "first",
                    "last",
                    "prev",
                    "next",
                ],
                "meta" => [
                    "current_page",
                    "from",
                    "last_page",
                    "path",
                    "per_page",
                    "to",
                    "total",
                ],
            ]);
    }

    /**
     * Test can show city
     */
    public function test_can_show_short_cities_list() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(City::class)->create();

        $this->get(route("api.admin.cities.list"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [],
            ]);
    }

    /**
     * Test can show city
     */
    public function test_can_show_city() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $city = factory(City::class)->create();

        $this->get(route("api.admin.cities.show", $city->id))
            ->assertStatus(200)
            ->assertJson(["data" => $city->toArray()]);
    }

    /**
     * Test can store city
     */
    public function test_can_store_city() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $data = [
            "name" => $this->faker->city,
        ];

        $this->post(route("api.admin.cities.store"), $data)
            ->assertStatus(201)
            ->assertJson(["data" => $data]);
    }

    /**
     * Test can update city
     */
    public function test_can_update_city() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $city = factory(City::class)->create();

        $data = [
            "name" => $this->faker->city,
        ];

        $this->put(route("api.admin.cities.update", $city->id), $data)
            ->assertStatus(200)
            ->assertJson(["data" => $data]);
    }

    /**
     * Test can destroy city
     */
    public function test_can_destroy_city() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $city = factory(City::class)->create();

        $this->delete(route("api.admin.cities.destroy", $city->id))
            ->assertStatus(204);
    }
}
