<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\Master;
use App\Rank;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MasterTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show masters list
     */
    public function test_can_show_masters_list_with_pagination() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(Master::class)->create();

        $this->get(route("api.admin.masters.index"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "name",
                        "surname",
                        "rating",
                        "rank_id",
                        "rank",
                        "image_id",
                        "image_url",
                        "created_at",
                        "updated_at",
                    ]
                ],
                "links" => [
                    "first",
                    "last",
                    "prev",
                    "next",
                ],
                "meta" => [
                    "current_page",
                    "from",
                    "last_page",
                    "path",
                    "per_page",
                    "to",
                    "total",
                ],
            ]);
    }

    /**
     * Test can show master
     */
    public function test_can_show_master() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $master = factory(Master::class)->create();

        $this->get(route("api.admin.masters.show", $master->id))
            ->assertStatus(200)
            ->assertJson(["data" => $master->toArray()]);
    }

    /**
     * Test can store master
     */
    public function test_can_store_master() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $data = [
            "name" => $this->faker->firstName,
            "surname" => $this->faker->lastName,
            "rating" => $this->faker->randomFloat(2, 0, 5),
            "rank_id" => null,
            "image_id" => null,
        ];

        $this->post(route("api.admin.masters.store"), $data)
            ->assertStatus(201)
            ->assertJson(["data" => $data]);
    }

    /**
     * Test can update master
     */
    public function test_can_update_master() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $master = factory(Master::class)->create();

        $data = [
            "name" => $this->faker->firstName,
            "surname" => $this->faker->lastName,
            "rating" => $this->faker->randomFloat(2, 0, 5),
            "rank_id" => null,
            "image_id" => null,
        ];

        $this->put(route("api.admin.masters.update", $master->id), $data)
            ->assertStatus(200)
            ->assertJson(["data" => $data]);
    }

    /**
     * Test can attach rank to master
     */
    public function test_can_attach_rank_to_master() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        /** @var Master $master */
        $master = factory(Master::class)->create();
        $rank = factory(Rank::class)->create();

        $master->update(["rank_id" => $rank->id]);

        $this->assertEquals($rank->toArray(), $master->rank->toArray());
    }

    /**
     * Test can destroy master
     */
    public function test_can_destroy_master() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        /** @var Master $master */
        $master = factory(Master::class)->create();
        $this->delete(route("api.admin.masters.destroy", $master->id))
            ->assertStatus(204);
    }
}
