<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\MasterBusyHour;
use App\Order;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show order
     */
    public function test_can_show_orders_list_with_pagination() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(Order::class)->create();

        $this->get(route("api.admin.orders.index"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    [
                        "id",
                        "comment",
                        "customer",
                        "master",
                    ]
                ],
                "links" => [
                    "first",
                    "last",
                    "prev",
                    "next",
                ],
                "meta" => [
                    "current_page",
                    "from",
                    "last_page",
                    "path",
                    "per_page",
                    "to",
                    "total",
                ],
            ]);
    }

    /**
     * Test can show order
     */
    public function test_can_show_order() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $order = factory(Order::class)->create();

        $this->get(route("api.admin.orders.show", $order->id))
            ->assertStatus(200)
            ->assertJsonStructure(["data" => [
                "id",
                "comment",
                "customer",
                "master",
                "master_busy_hours",
                "created_at",
                "updated_at",
            ]]);
    }

    /**
     * Test can update order
     */
    public function test_can_update_order() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        $order = factory(Order::class)->create();

        $data = [
            "comment" => $this->faker->text(),
        ];

        $this->put(route("api.admin.orders.update", $order->id), $data)
            ->assertStatus(200)
            ->assertJson(["data" => $data]);
    }

    /**
     * Test can destroy order
     */
    public function test_can_destroy_order() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        /** @var Order $order */
        $order = factory(Order::class)->create();
        $masterBusyHours = $order->masterBusyHours;

        $this->delete(route("api.admin.orders.destroy", $order->id))
            ->assertStatus(204);
        $this->assertNotNull(MasterBusyHour::query()->withTrashed()->find($masterBusyHours->id)->deleted_at);
    }
}
