<?php

namespace Modules\Admin\Tests\Feature\Admin;

use App\Rank;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RankTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Test can show Ranks short list
     */
    public function test_can_show_ranks_short_list() {
        $user = factory(User::class)->create();
        $this->actingAs($user, "api");

        factory(Rank::class, 3)->create();
        $ranks = Rank::all();

        $this->get(route("api.admin.ranks.list"))
            ->assertStatus(200)
            ->assertJson([
                "data" => $ranks->pluck("title", "id")->toArray()
            ])
;
    }
}
