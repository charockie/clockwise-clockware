export default {
  items: [
    {
      name: 'Инфо панель',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Города',
      url: '/cities',
      icon: 'fa fa-building fa-lg',
    },
    {
      name: 'Мастера',
      url: '/masters',
      icon: 'fa fa-users fa-lg',
    },
    {
      name: 'Клиенты',
      url: '/customers',
      icon: 'fa fa-users fa-lg',
    },
    {
      name: 'Заказы',
      url: '/orders',
      icon: 'fa fa-book fa-lg',
    },
  ],
};
