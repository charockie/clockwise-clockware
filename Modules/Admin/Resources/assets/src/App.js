import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.scss';
import { connect } from 'react-redux';
import Loading from './containers/Loading';

const loading = () => <Loading/>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const LoginContainer = React.lazy(() => import('./views/Login/LoginContainer'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          {!this.props.isAuthenticated ? (
            <Route name="Login Page" render={props => <LoginContainer {...props}/>} />
          ) : (
            <Switch>
              <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
            </Switch>
          )
          }
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.login.isAuthenticated
  };
}

const mapDispatchToProps = {

}

export default App = connect(mapStateToProps, mapDispatchToProps)(App);
