import React, { Component } from 'react';
import {
  Button, ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import CardImg from 'reactstrap/es/CardImg';

class Show extends Component {
  render() {
    const { id, name, surname, rating, rank, image_url, cities_names, created_at, updated_at } = this.props.master;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="6">
            <Card>
              <CardHeader>
                Просмотр мастера №{id}

                <ButtonGroup className="pull-right">
                  <Link to={`/masters/${id}/edit`} className="btn btn-primary" title="Редактировать"><i className="fa fa-edit"></i></Link>
                  <Button onClick={e => this.props.onDelete(id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                </ButtonGroup>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    { image_url ? (
                      <CardImg src={image_url} alt="Фото" />
                    ) : (
                      <p>Фото не загружено</p>
                    )}
                  </Col>
                  <Col xs="12" md="6" xl="6">
                    <p>Имя мастера: {name} {surname}</p>
                    <p>Рейтинг: {rating}</p>
                    <p>Ранг: {rank}</p>
                    <p>Города: {cities_names.join(", ")}</p>
                    <p>Добавлен: {created_at}</p>
                    <p>Последнее изменение: {updated_at}</p>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Show;
