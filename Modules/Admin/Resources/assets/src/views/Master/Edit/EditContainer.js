import React, { Component } from 'react';
import { editMaster, updateMaster } from '../../../store/Master/actions';
import { connect } from 'react-redux';
import Edit from './Edit';
import Loading from '../../../containers/Loading';

class EditContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    const masterId = this.props.match.params.id;
    this.props.editMaster(masterId);
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.updateMaster(
      this.props.form.id,
      this.props.form,
      (id) => this.props.history.push(`/masters/${id}`)
    );
  }

  render() {
    if (!this.props.form || !this.props.form.id)
      return <Loading/>;

    return (
      <Edit
        onSubmit={this.onSubmit}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    form: state.master.form,
  };
}

const mapDispatchToProps = {
  editMaster,
  updateMaster,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
