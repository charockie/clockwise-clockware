import React, { Component } from 'react';
import Create from './Create';
import { connect } from 'react-redux';
import { createMaster, storeMaster } from '../../../store/Master/actions';

class CreateContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.createMaster();
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.storeMaster(
      this.props.form,
      (id) => this.props.history.push(`/masters/${id}`)
    );
  }

  render() {
    return (
      <Create
        onSubmit={this.onSubmit}
      />
    );
  }
}


const mapStateToProps = state => {
  return {
    form: state.master.form
  };
}

const mapDispatchToProps = {
  createMaster,
  storeMaster,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer);
