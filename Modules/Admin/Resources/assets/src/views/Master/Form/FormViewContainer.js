import React, { Component } from 'react';

import PropTypes from 'prop-types';
import FormView from './FormView';
import { changeForm, resetForm, uploadMasterImage, deleteMasterImage } from '../../../store/Master/actions';
import { fetchRanksList } from '../../../store/Rank/actions';
import { fetchCitiesShortList } from '../../../store/City/actions';
import { connect } from 'react-redux';
import { createLoadingSelector, createValidationErrorsListSelector } from '../../../store/api/selectors';
import { resetValidationErrors } from '../../../store/api/actions';
import Loading from '../../../containers/Loading';

class FormViewContainer extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onChangeCities = this.onChangeCities.bind(this);
    this.onFileChange = this.onFileChange.bind(this);
    this.onFileDelete = this.onFileDelete.bind(this);
    this.onReset = this.onReset.bind(this);
    this.getError = this.getError.bind(this);
    this.hasError = this.hasError.bind(this);
  }

  componentWillMount() {
    this.props.fetchRanksList();
    this.props.fetchCitiesShortList();
  }

  componentWillUnmount() {
    this.onReset();
  }

  onChange(event) {
    this.props.changeForm(event.target.name, event.target.value);
  }

  onChangeCities(event) {
    let options = event.target.options;
    let cities = [];
    for (let i = 0, l = options.length; i < l; i++) {
      if (options[i].selected) {
        cities.push(options[i].value);
      }
    }

    this.props.changeForm(event.target.name, cities);
  }

  onFileChange(event) {
    const file = event.target.files[0];

    this.props.uploadMasterImage(file);
  }

  onFileDelete(event) {
    this.props.deleteMasterImage();
  }

  onReset() {
    this.props.resetForm(!!this.props.form.id);
  }

  hasError(name) {
    return this.props.errors && this.props.errors[name] && this.props.errors[name].length !== 0;
  }

  getError(name) {
    if (!this.hasError(name))
      return;

    return this.props.errors[name][0];
  }

  render() {
    if (this.props.isLoadingFormData)
      return <Loading/>;

    const { id, name, surname, rating, rank_id, cities, image_id, image_url } = this.props.form;

    return (
      <FormView
        id={id}
        name={name}
        surname={surname}
        rating={rating}
        rank_id={rank_id ? rank_id : ''}
        image_id={image_id ? image_id : ''}
        image_url={image_url ? image_url : ''}
        cities={cities}

        ranksList={this.props.ranksList}
        citiesList={this.props.citiesList}

        onChange={this.onChange}
        onChangeCities={this.onChangeCities}
        onFileChange={this.onFileChange}
        onFileDelete={this.onFileDelete}
        onReset={this.onReset}
        onSubmit={this.props.onSubmit}
        isLoading={this.props.isLoading}

        hasError={this.hasError}
        getError={this.getError}
      />
    );
  }
}

FormViewContainer.propTypes = {
  form: PropTypes.object.isRequired,
  ranksList: PropTypes.object.isRequired,

  errors: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,

  onSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
};


const validationErrorsListSelector = createValidationErrorsListSelector(['STORE_MASTER', 'UPDATE_MASTER', 'UPLOAD_MASTER_IMAGE']);
const loadingSelector = createLoadingSelector(['STORE_MASTER', 'UPDATE_MASTER']);
const formDataLoadingSelector = createLoadingSelector(['GET_RANKS_LIST', 'GET_CITIES_LIST']);
const mapStateToProps = state => {
  return {
    form: state.master.form,
    ranksList: state.rank.list,
    citiesList: state.city.shortList,
    errors: validationErrorsListSelector(state),
    isLoading: loadingSelector(state),
    isLoadingFormData: formDataLoadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchRanksList,
  fetchCitiesShortList,
  changeForm,
  resetForm,
  resetValidationErrors,
  uploadMasterImage,
  deleteMasterImage,
}

export default connect(mapStateToProps, mapDispatchToProps)(FormViewContainer);
