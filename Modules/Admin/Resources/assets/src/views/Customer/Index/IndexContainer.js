import React, { Component } from 'react';
import Index from './Index';
import { connect } from 'react-redux';
import queryString from 'querystring';
import { fetchCustomersList, changePage, deleteCustomer } from '../../../store/Customer/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class IndexContainer extends Component {
  constructor(props) {
    super(props);

    this.changePage = this.changePage.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    this.loadPageData();
  }

  loadPageData() {
    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.fetchCustomersList(query.page);
  }

  changePage(page) {
    this.props.changePage(page);

    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.history.push({
      search: queryString.stringify({
        ...query,
        page: page,
      }),
    });
  }

  onDelete(id) {
    this.props.deleteCustomer(
      id,
      () => this.loadPageData()
    );
  }

  render() {
    if (this.props.isFetching)
      return <Loading/>;

    return (
      <Index
        data={this.props.data}
        meta={this.props.meta}
        changePage={this.changePage}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_CUSTOMERS']);
const mapStateToProps = state => {
  return {
    data: state.customer.list.data,
    meta: state.customer.list.meta,
    isFetching: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchCustomersList,
  changePage,
  deleteCustomer,
}

export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);
