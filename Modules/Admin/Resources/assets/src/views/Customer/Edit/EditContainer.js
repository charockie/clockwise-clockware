import React, { Component } from 'react';
import { editCustomer, updateCustomer } from '../../../store/Customer/actions';
import { connect } from 'react-redux';
import Edit from './Edit';
import Loading from '../../../containers/Loading';

class EditContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    const customerId = this.props.match.params.id;
    this.props.editCustomer(customerId);
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.updateCustomer(
      this.props.form.id,
      this.props.form,
      (id) => this.props.history.push(`/customers/${id}`)
    );
  }

  render() {
    if (!this.props.form || !this.props.form.id)
      return <Loading/>;

    return (
      <Edit
        onSubmit={this.onSubmit}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    form: state.customer.form,
  };
}

const mapDispatchToProps = {
  editCustomer,
  updateCustomer,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
