import React, { Component } from 'react';
import {
  Button, ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import { Link } from 'react-router-dom';

class Show extends Component {
  render() {
    const { id, name, email, city, created_at, updated_at } = this.props.customer;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="6">
            <Card>
              <CardHeader>
                Просмотр клиента №{id}

                <ButtonGroup className="pull-right">
                  <Link to={`/customers/${id}/edit`} className="btn btn-primary" title="Редактировать"><i className="fa fa-edit"></i></Link>
                  <Button onClick={e => this.props.onDelete(id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                </ButtonGroup>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    <p>Имя: {name}</p>
                    <p>Email: {email}</p>
                    <p>Город: {city.name}</p>
                    <p>Добавлен: {created_at}</p>
                    <p>Последнее изменение: {updated_at}</p>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Show;
