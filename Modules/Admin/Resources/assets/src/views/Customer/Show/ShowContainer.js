import React, { Component } from 'react';
import Show from './Show';
import { connect } from 'react-redux';
import { fetchCustomer, deleteCustomer } from '../../../store/Customer/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class ShowContainer extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    const customerId = this.props.match.params.id;
    this.props.fetchCustomer(customerId);
  }

  onDelete(id) {
    this.props.deleteCustomer(
      id,
      () => this.props.history.push(`/customers`)
    );
  }

  render() {
    if (this.props.isFetching || !this.props.customer)
      return <Loading/>;

    return (
      <Show
        customer={this.props.customer}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_CUSTOMER']);
const mapStateToProps = state => {
  return {
    customer: state.customer.show,
    isLoading: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchCustomer,
  deleteCustomer,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowContainer);

