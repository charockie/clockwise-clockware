import React, { Component } from 'react';
import { editOrder, updateOrder } from '../../../store/Order/actions';
import { connect } from 'react-redux';
import Edit from './Edit';
import Loading from '../../../containers/Loading';

class EditContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    const orderId = this.props.match.params.id;
    this.props.editOrder(orderId);
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.updateOrder(
      this.props.form.id,
      this.props.form,
      (id) => this.props.history.push(`/orders/${id}`)
    );
  }

  render() {
    if (!this.props.form || !this.props.form.id)
      return <Loading/>;

    return (
      <Edit
        onSubmit={this.onSubmit}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    form: state.order.form,
  };
}

const mapDispatchToProps = {
  editOrder,
  updateOrder,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
