import React, { Component } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  ButtonGroup,
  FormFeedback,
} from 'reactstrap';
import PropTypes from 'prop-types';

class FormView extends Component {
  render() {
    const { onSubmit, onReset, onChange, isLoading, hasError, getError, id, comment } = this.props;

    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <h3>
              <strong>Редактирование</strong> заказа №{ id }
            </h3>
          </CardHeader>
          <CardBody>
            <Form onSubmit={onSubmit} className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="comment">Комментарий</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text"
                         id="comment"
                         name="comment"
                         value={comment}
                         onChange={onChange}
                         placeholder="Введите комментарий..."
                         invalid={ hasError("comment") }
                  />
                  <FormFeedback>{ getError("comment") }</FormFeedback>
                </Col>
              </FormGroup>

            </Form>
          </CardBody>
          <CardFooter className="text-right">
            <ButtonGroup size="sm">
              <Button type="reset" onClick={onReset} color="danger" disabled={isLoading}><i className="fa fa-ban"></i> Сброс</Button>
              <Button type="submit" onClick={onSubmit} color="success" disabled={isLoading}><i className="fa fa-dot-circle-o"></i> Сохранить</Button>
            </ButtonGroup>
          </CardFooter>
        </Card>

      </div>
    );
  }
}

FormView.propTypes = {
  id: PropTypes.number,
  comment: PropTypes.string,

  isLoading: PropTypes.bool.isRequired,

  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  hasError: PropTypes.func.isRequired,
  getError: PropTypes.func.isRequired,
};

export default FormView;
