import React, { Component } from 'react';

import PropTypes from 'prop-types';
import FormView from './FormView';
import { changeForm, resetForm } from '../../../store/Order/actions';
import { connect } from 'react-redux';
import { createLoadingSelector, createValidationErrorsListSelector } from '../../../store/api/selectors';
import { resetValidationErrors } from '../../../store/api/actions';

class FormViewContainer extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onReset = this.onReset.bind(this);
    this.getError = this.getError.bind(this);
    this.hasError = this.hasError.bind(this);
  }

  componentWillUnmount() {
    this.onReset();
  }

  onChange(event) {
    this.props.changeForm(event.target.name, event.target.value);
  }

  onReset() {
    this.props.resetForm(!!this.props.form.id);
  }

  hasError(name) {
    return this.props.errors && this.props.errors[name] && this.props.errors[name].length !== 0;
  }

  getError(name) {
    if (!this.hasError(name))
      return;

    return this.props.errors[name][0];
  }

  render() {
    const { id, comment } = this.props.form;

    return (
      <FormView
        id={id}
        comment={comment}

        onChange={this.onChange}
        onReset={this.onReset}
        onSubmit={this.props.onSubmit}
        isLoading={this.props.isLoading}

        hasError={this.hasError}
        getError={this.getError}
      />
    );
  }
}

FormViewContainer.propTypes = {
  form: PropTypes.object.isRequired,

  errors: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,

  onSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
};


const validationErrorsListSelector = createValidationErrorsListSelector(['UPDATE_ORDER']);
const loadingSelector = createLoadingSelector(['UPDATE_ORDER']);
const mapStateToProps = state => {
  return {
    form: state.order.form,
    errors: validationErrorsListSelector(state),
    isLoading: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  changeForm,
  resetForm,
  resetValidationErrors,
}

export default connect(mapStateToProps, mapDispatchToProps)(FormViewContainer);
