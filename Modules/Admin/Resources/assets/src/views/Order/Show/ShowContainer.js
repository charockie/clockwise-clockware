import React, { Component } from 'react';
import Show from './Show';
import { connect } from 'react-redux';
import { fetchOrder, deleteOrder } from '../../../store/Order/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class ShowContainer extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    const orderId = this.props.match.params.id;
    this.props.fetchOrder(orderId);
  }

  onDelete(id) {
    this.props.deleteOrder(
      id,
      () => this.props.history.push(`/orders`)
    );
  }

  render() {
    if (this.props.isFetching || !this.props.order)
      return <Loading/>;

    return (
      <Show
        order={this.props.order}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_ORDER']);
const mapStateToProps = state => {
  return {
    order: state.order.show,
    isLoading: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchOrder,
  deleteOrder,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowContainer);

