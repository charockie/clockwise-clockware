import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import { Link } from 'react-router-dom';

class Show extends Component {
  render() {
    const { id, comment, is_confirmed, customer, master, master_busy_hours, created_at, updated_at } = this.props.order;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="6">
            <Card>
              <CardHeader>
                Просмотр заказа №{id}

                <ButtonGroup className="pull-right">
                  <Link to={`/orders/${id}/edit`} className="btn btn-primary" title="Редактировать"><i className="fa fa-edit"></i></Link>
                  <Button onClick={e => this.props.onDelete(id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                </ButtonGroup>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12" xl="12">
                    <p>Клиент: {customer.name}</p>
                    <p>Мастер: {master.name} {master.surname}</p>
                    <p>Город: {customer.city.name}</p>
                    <p>
                      Время работы: {master_busy_hours.started_at} - {master_busy_hours.ended_at}
                    </p>
                    <p>Комментарий: {comment}</p>
                    <p>Статус:
                      { is_confirmed ?
                        <Badge color="success">Подтвержден</Badge>
                        :
                        <Badge color="danger">Не подтвержден</Badge>
                      }
                    </p>
                    <p>Добавлен: {created_at}</p>
                    <p>Последнее изменение: {updated_at}</p>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Show;
