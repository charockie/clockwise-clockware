import React, { Component } from 'react';
import Index from './Index';
import { connect } from 'react-redux';
import queryString from 'querystring';
import { fetchOrdersList, changePage, deleteOrder } from '../../../store/Order/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class IndexContainer extends Component {
  constructor(props) {
    super(props);

    this.changePage = this.changePage.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    this.loadPageData();
  }

  loadPageData() {
    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.fetchOrdersList(query.page);
  }

  changePage(page) {
    this.props.changePage(page);

    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.history.push({
      search: queryString.stringify({
        ...query,
        page: page,
      }),
    });
  }

  onDelete(id) {
    this.props.deleteOrder(
      id,
      () => this.loadPageData()
    );
  }

  render() {
    if (this.props.isFetching)
      return <Loading/>;

    return (
      <Index
        data={this.props.data}
        meta={this.props.meta}
        changePage={this.changePage}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_ORDERS']);
const mapStateToProps = state => {
  return {
    data: state.order.list.data,
    meta: state.order.list.meta,
    isFetching: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchOrdersList,
  changePage,
  deleteOrder,
}

export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);
