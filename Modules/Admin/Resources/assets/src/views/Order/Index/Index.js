import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  ButtonGroup,
  Badge,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { Pagination } from '../../../containers';

class Index extends Component {
  render() {
    const {current_page, last_page, total } = this.props.meta;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Заказы ({ total })
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Клиент</th>
                    <th>Мастер</th>
                    <th>Город</th>
                    <th>Комментарий</th>
                    <th>Статус</th>
                    <th>Время работы</th>
                    <th>Дата создания</th>
                    <th>Дата редактирования</th>
                    <th className="text-right">Действия</th>
                  </tr>
                  </thead>
                  <tbody>

                  { this.props.data.length ? (
                    this.props.data.map((order) => {
                      return (
                        <tr key={ order.id }>
                          <td>{ order.id }</td>
                          <td>{ order.customer.name } <br/> { order.customer.email }</td>
                          <td>{ order.master.name } { order.master.surname }</td>
                          <td>{ order.customer.city.name }</td>
                          <td>{ order.comment }</td>
                          <td>
                            { order.is_confirmed ?
                              <Badge color="success">Подтвержден</Badge>
                            :
                              <Badge color="danger">Не подтвержден</Badge>
                            }
                          </td>
                          <td className="text-nowrap">
                            <span title="Начало">Н: { order.master_busy_hours.started_at }</span>
                            <br/>
                            <span title="Конец">К: { order.master_busy_hours.ended_at }</span>
                          </td>
                          <td className="text-nowrap">{ order.created_at }</td>
                          <td className="text-nowrap">{ order.updated_at }</td>
                          <td className="text-right">
                            <ButtonGroup>
                              <Link to={`orders/${order.id}`} className="btn btn-success" title="Просмотр"><i className="fa fa-eye"></i></Link>
                              <Link to={`orders/${order.id}/edit`} className="btn btn-primary" title="Редактирова"><i className="fa fa-edit"></i></Link>
                              <Button onClick={e => this.props.onDelete(order.id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                            </ButtonGroup>
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} align="center">Данных нет.</td>
                    </tr>
                  ) }

                  </tbody>
                </Table>

                <Pagination
                  page={current_page}
                  last={last_page}
                  onClick={this.props.changePage}
                />

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

Index.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    comment: PropTypes.string,
    is_confirmed: PropTypes.bool.isRequired,
    customer: PropTypes.object.isRequired,
    master: PropTypes.object.isRequired,
    master_busy_hours: PropTypes.object.isRequired,
    created_at: PropTypes.string.isRequired,
    updated_at: PropTypes.string.isRequired,
  })).isRequired,
  meta: PropTypes.shape({
    current_page: PropTypes.number.isRequired,
    from: PropTypes.number,
    last_page: PropTypes.number.isRequired,
    path: PropTypes.string.isRequired,
    per_page: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    to: PropTypes.number,
    total: PropTypes.number.isRequired,
  }).isRequired,
  changePage: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default Index;
