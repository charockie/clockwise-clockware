import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  ButtonGroup,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { Pagination } from '../../../containers';

class Index extends Component {
  render() {
    const {current_page, last_page, total } = this.props.meta;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <Link to="/cities/create" className="btn btn-success pull-right" title="Добаить город">
                  <i className="fa fa-plus"></i>
                </Link>

                <i className="fa fa-align-justify"></i> Города ({ total })
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>Название</th>
                    <th>Дата создания</th>
                    <th>Дата редактирования</th>
                    <th className="text-right">Действия</th>
                  </tr>
                  </thead>
                  <tbody>

                  { this.props.data.length ? (
                    this.props.data.map((city) => {
                      return (
                        <tr key={ city.id }>
                          <td>{ city.name }</td>
                          <td>{ city.created_at }</td>
                          <td>{ city.updated_at }</td>
                          <td className="text-right">
                            <ButtonGroup>
                              <Link to={`cities/${city.id}`} className="btn btn-success" title="Просмотр"><i className="fa fa-eye"></i></Link>
                              <Link to={`cities/${city.id}/edit`} className="btn btn-primary" title="Редактирова"><i className="fa fa-edit"></i></Link>
                              <Button onClick={e => this.props.onDelete(city.id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                            </ButtonGroup>
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} align="center">Данных нет.</td>
                    </tr>
                  ) }

                  </tbody>
                </Table>

                <Pagination
                  page={current_page}
                  last={last_page}
                  onClick={this.props.changePage}
                />

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

Index.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    created_at: PropTypes.string.isRequired,
    updated_at: PropTypes.string.isRequired,
  })).isRequired,
  meta: PropTypes.shape({
    current_page: PropTypes.number.isRequired,
    from: PropTypes.number,
    last_page: PropTypes.number.isRequired,
    path: PropTypes.string.isRequired,
    per_page: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    to: PropTypes.number,
    total: PropTypes.number.isRequired,
  }).isRequired,
  changePage: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default Index;
