import React, { Component } from 'react';
import Index from './Index';
import { connect } from 'react-redux';
import queryString from 'querystring';
import { fetchCitiesList, changePage, deleteCity } from '../../../store/City/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class IndexContainer extends Component {
  constructor(props) {
    super(props);

    this.changePage = this.changePage.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    this.loadPageData();
  }

  loadPageData() {
    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.fetchCitiesList(query.page);
  }

  changePage(page) {
    this.props.changePage(page);

    const query = queryString.parse(this.props.location.search.replace('?', ''));
    this.props.history.push({
      search: queryString.stringify({
        ...query,
        page: page,
      }),
    });
  }

  onDelete(id) {
    this.props.deleteCity(
      id,
      () => this.loadPageData()
    );
  }

  render() {
    if (this.props.isFetching)
      return <Loading/>;

    return (
      <Index
        data={this.props.data}
        meta={this.props.meta}
        changePage={this.changePage}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_CITIES']);
const mapStateToProps = state => {
  return {
    data: state.city.list.data,
    meta: state.city.list.meta,
    isFetching: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchCitiesList,
  changePage,
  deleteCity,
}

export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);
