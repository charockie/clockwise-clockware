import React, { Component } from 'react';
import {
  Col,
  Row,
} from 'reactstrap';
import FormViewContainer from '../Form/FormViewContainer';

class Edit extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="6" xl="6">

            <FormViewContainer {...this.props} />

          </Col>
        </Row>
      </div>
    );
  }
}

export default Edit;
