import React, { Component } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  ButtonGroup,
  FormFeedback,
} from 'reactstrap';
import PropTypes from 'prop-types';

class FormView extends Component {
  render() {
    const { onSubmit, onReset, onChange, isLoading, hasError, getError, id, name } = this.props;

    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            {id ? (
              <h3>
                <strong>Редактирование</strong> города №{ id }
              </h3>
            ) : (
              <h3>
                <strong>Добавление</strong> нового города
              </h3>
              )
            }
          </CardHeader>
          <CardBody>
            <Form onSubmit={onSubmit} className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="name">Название</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text"
                         id="name"
                         name="name"
                         value={name}
                         onChange={onChange}
                         placeholder="Введите название..."
                         invalid={ hasError("name") }
                  />
                  <FormFeedback>{ getError("name") }</FormFeedback>
                </Col>
              </FormGroup>
            </Form>
          </CardBody>
          <CardFooter className="text-right">
            <ButtonGroup size="sm">
              <Button type="reset" onClick={onReset} color="danger" disabled={isLoading}><i className="fa fa-ban"></i> Сброс</Button>
              <Button type="submit" onClick={onSubmit} color="success" disabled={isLoading}><i className="fa fa-dot-circle-o"></i> Сохранить</Button>
            </ButtonGroup>
          </CardFooter>
        </Card>

      </div>
    );
  }
}

FormView.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,

  isLoading: PropTypes.bool.isRequired,

  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  hasError: PropTypes.func.isRequired,
  getError: PropTypes.func.isRequired,
};

export default FormView;
