import React, { Component } from 'react';
import Create from './Create';
import { connect } from 'react-redux';
import { storeCity } from '../../../store/City/actions';

class CreateContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.storeCity(
      this.props.form,
      (id) => this.props.history.push(`/cities/${id}`)
    );
  }

  render() {
    return (
      <Create
        onSubmit={this.onSubmit}
      />
    );
  }
}


const mapStateToProps = state => {
  return {
    form: state.city.form
  };
}

const mapDispatchToProps = {
  storeCity
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer);
