import React, { Component } from 'react';

class Loading extends Component {
  render() {
    return (
      <div className="animated fadeIn pt-1 text-center">Загрузка...</div>
    );
  }
}

export default Loading;
