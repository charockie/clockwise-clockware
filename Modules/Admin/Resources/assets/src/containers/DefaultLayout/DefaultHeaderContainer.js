import React, { Component } from 'react';
import { logout } from '../../store/Login/actions';
import { connect } from 'react-redux';
import DefaultHeader from './DefaultHeader';

class DefaultHeaderContainer extends Component {
  render() {
    return (
      <DefaultHeader logout={this.props.logout} />
    );
  }
}

const mapStateToProps = state => {
  return {};
}

const mapDispatchToProps = {
  logout,
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeaderContainer);
