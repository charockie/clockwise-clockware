import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));

const CitiesIndex = React.lazy(() => import('./views/City/Index/IndexContainer'));
const CitiesShow = React.lazy(() => import('./views/City/Show/ShowContainer'));
const CitiesCreate = React.lazy(() => import('./views/City/Create/CreateContainer'));
const CitiesEdit = React.lazy(() => import('./views/City/Edit/EditContainer'));

const MastersIndex = React.lazy(() => import('./views/Master/Index/IndexContainer'));
const MastersShow = React.lazy(() => import('./views/Master/Show/ShowContainer'));
const MastersCreate = React.lazy(() => import('./views/Master/Create/CreateContainer'));
const MastersEdit = React.lazy(() => import('./views/Master/Edit/EditContainer'));

const CustomersIndex = React.lazy(() => import('./views/Customer/Index/IndexContainer'));
const CustomersShow = React.lazy(() => import('./views/Customer/Show/ShowContainer'));
const CustomersEdit = React.lazy(() => import('./views/Customer/Edit/EditContainer'));

const OrdersIndex = React.lazy(() => import('./views/Order/Index/IndexContainer'));
const OrdersShow = React.lazy(() => import('./views/Order/Show/ShowContainer'));
const OrdersEdit = React.lazy(() => import('./views/Order/Edit/EditContainer'));

const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Главная' },
  { path: '/dashboard', name: 'Инфо панель', component: Dashboard },

  { path: '/cities', exact: true, name: 'Города', component: CitiesIndex },
  { path: '/cities/create', exact: true, name: 'Добавление города', component: CitiesCreate },
  { path: '/cities/:id', exact: true, name: 'Просмотр города', component: CitiesShow },
  { path: '/cities/:id/edit', exact: true, name: 'Редактирование города', component: CitiesEdit },

  { path: '/masters', exact: true, name: 'Мастера', component: MastersIndex },
  { path: '/masters/create', exact: true, name: 'Добавление мастера', component: MastersCreate },
  { path: '/masters/:id', exact: true, name: 'Просмотр мастера', component: MastersShow },
  { path: '/masters/:id/edit', exact: true, name: 'Редактирование мастера', component: MastersEdit },

  { path: '/customers', exact: true, name: 'Клиенты', component: CustomersIndex },
  { path: '/customers/:id', exact: true, name: 'Просмотр клиента', component: CustomersShow },
  { path: '/customers/:id/edit', exact: true, name: 'Редактирование клиента', component: CustomersEdit },

  { path: '/orders', exact: true, name: 'Заказы', component: OrdersIndex },
  { path: '/orders/:id', exact: true, name: 'Просмотр заказа', component: OrdersShow },
  { path: '/orders/:id/edit', exact: true, name: 'Редактирование заказа', component: OrdersEdit },

  { path: '/not-found', name: 'Not Found', component: Page404 },
  { path: '/server-error', name: 'Server Error', component: Page500 },
];

export default routes;
