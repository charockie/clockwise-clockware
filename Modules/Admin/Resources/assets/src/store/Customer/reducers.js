import {
  GET_CUSTOMERS_SUCCESS,
  GET_CUSTOMER_SUCCESS,
  EDIT_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_SUCCESS,
  DELETE_CUSTOMER_SUCCESS,

  CHANGE_PAGE,
  CHANGE_FORM,
  RESET_FORM,
} from './actions';

const defaultState = {
  list: {
    data: [],
    links: {},
    meta: {
      current_page: 1,
      from: null,
      last_page: 1,
      path: "",
      per_page: 10,
      to: null,
      total: 0,
    },
  },
  show: null,
  form: {
    id: null,
    name: '',
    email: '',
    city_id: '',
  },
}

export const customerReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_CUSTOMERS_SUCCESS:
      return { ...state,
        list: action.payload
      };

    case GET_CUSTOMER_SUCCESS:
      return { ...state,
        show: action.payload
      };

    case CHANGE_PAGE:
      return setPage(state, action.payload);

    case CHANGE_FORM:
      const {name, value} = action.payload;
      return { ...state,
        form: {...state.form, [name]: value}
      };

    case RESET_FORM:
      return { ...state,
        form: {...defaultState.form,
          id: action.payload ? state.form.id : null
        }
      };

    case EDIT_CUSTOMER_SUCCESS:
      return { ...state,
        form: { ...state.form,
          id: action.payload.id,
          name: action.payload.name,
          email: action.payload.email,
          city_id: action.payload.city.id ? action.payload.city.id : '',
        }
      };

    case UPDATE_CUSTOMER_SUCCESS:
      return state;

    case DELETE_CUSTOMER_SUCCESS:
      return { ...state,
        list: { ...state.list,
          data: [
            ...state.list.data.filter(item => item.id !== action.payload)
          ],
          meta: { ...state.list.meta,
            total: --state.list.meta.total
          }
        }
      };

    default:
      return state;
  }
}

function setPage(state, newPage) {
  return { ...state,
    list: { ...state.list,
      meta: { ...state.list.meta,
        current_page: newPage
      }
    }
  }
}
