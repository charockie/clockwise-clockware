const initial = {
  loading: [],
};
export const loadingReducer = (state = initial, action) => {
  const { type } = action;
  const matches = /(.*)_(REQUEST|SUCCESS|FAILURE)/.exec(type);

  // not a *_REQUEST / *_SUCCESS /  *_FAILURE actions, so we ignore them
  if (!matches) return state;

  const [, requestName, requestState] = matches;
  return {
    // Store whether a request is happening at the moment or not
    [requestName]: requestState === 'REQUEST'
  };
};
