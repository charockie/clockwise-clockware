import {
  GET_MASTERS_SUCCESS,
  GET_MASTER_SUCCESS,
  STORE_MASTER_SUCCESS,
  EDIT_MASTER_SUCCESS,
  UPDATE_MASTER_SUCCESS,
  DELETE_MASTER_SUCCESS,

  CHANGE_PAGE,
  CHANGE_FORM,
  RESET_FORM, UPLOAD_MASTER_IMAGE_SUCCESS, DELETE_MASTER_IMAGE_SUCCESS, CREATE_MASTER_SUCCESS,
} from './actions';

const defaultState = {
  list: {
    data: [],
    links: {},
    meta: {
      current_page: 1,
      from: null,
      last_page: 1,
      path: "",
      per_page: 10,
      to: null,
      total: 0,
    },
  },
  show: null,
  form: {
    id: null,
    name: '',
    surname: '',
    rating: '',
    rank_id: null,
    cities: [],
    image: null,
    image_id: null,
    image_url: null,
  },
}

export const masterReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_MASTERS_SUCCESS:
      return { ...state,
        list: action.payload
      };

    case GET_MASTER_SUCCESS:
      return { ...state,
        show: action.payload
      };

    case CHANGE_PAGE:
      return setPage(state, action.payload);

    case CHANGE_FORM:
      const {name, value} = action.payload;
      return { ...state,
        form: {...state.form, [name]: value}
      };

    case RESET_FORM:
      return { ...state,
        form: {...defaultState.form,
          id: action.payload ? state.form.id : null
        }
      };

    case CREATE_MASTER_SUCCESS:
      return {...state, form: defaultState.form};

    case STORE_MASTER_SUCCESS:
      state.list.data.push(action.payload);
      return state;

    case EDIT_MASTER_SUCCESS:
      return { ...state,
        form: { ...state.form,
          id: action.payload.id,
          name: action.payload.name,
          surname: action.payload.surname,
          rating: action.payload.rating,
          rank_id: action.payload.rank_id ? action.payload.rank_id : null,
          image_id: action.payload.image_id ? action.payload.image_id : null,
          image_url: action.payload.image_url ? action.payload.image_url : null,
          cities: action.payload.cities,
        }
      };

    case UPDATE_MASTER_SUCCESS:
      return state;

    case DELETE_MASTER_SUCCESS:
      return { ...state,
        list: { ...state.list,
          data: [
            ...state.list.data.filter(item => item.id !== action.payload)
          ],
          meta: { ...state.list.meta,
            total: --state.list.meta.total
          }
        }
      };

    case UPLOAD_MASTER_IMAGE_SUCCESS:
      return { ...state,
        form: { ...state.form,
          image_id: action.payload.id ? action.payload.id : null,
          image_url: action.payload.url ? action.payload.url : null,
        }
      };

    case DELETE_MASTER_IMAGE_SUCCESS:
      return { ...state,
        form: { ...state.form,
          image_id: null,
          image_url: null,
        }
      };

    default:
      return state;
  }
}

function setPage(state, newPage) {
  return { ...state,
    list: { ...state.list,
      meta: { ...state.list.meta,
        current_page: newPage
      }
    }
  }
}
