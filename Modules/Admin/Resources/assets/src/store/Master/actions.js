import axios from 'axios';

export const GET_MASTERS_REQUEST = 'GET_MASTERS_REQUEST';
export const GET_MASTERS_SUCCESS = 'GET_MASTERS_SUCCESS';
export const GET_MASTERS_FAILURE = 'GET_MASTERS_FAILURE';

export const GET_MASTER_REQUEST = 'GET_MASTER_REQUEST';
export const GET_MASTER_SUCCESS = 'GET_MASTER_SUCCESS';
export const GET_MASTER_FAILURE = 'GET_MASTER_FAILURE';

export const CREATE_MASTER_SUCCESS = 'CREATE_MASTER_SUCCESS';

export const STORE_MASTER_REQUEST = 'STORE_MASTER_REQUEST';
export const STORE_MASTER_SUCCESS = 'STORE_MASTER_SUCCESS';
export const STORE_MASTER_FAILURE = 'STORE_MASTER_FAILURE';

export const EDIT_MASTER_REQUEST = 'EDIT_MASTER_REQUEST';
export const EDIT_MASTER_SUCCESS = 'EDIT_MASTER_SUCCESS';
export const EDIT_MASTER_FAILURE = 'EDIT_MASTER_FAILURE';

export const UPDATE_MASTER_REQUEST = 'UPDATE_MASTER_REQUEST';
export const UPDATE_MASTER_SUCCESS = 'UPDATE_MASTER_SUCCESS';
export const UPDATE_MASTER_FAILURE = 'UPDATE_MASTER_FAILURE';

export const DELETE_MASTER_REQUEST = 'DELETE_MASTER_REQUEST';
export const DELETE_MASTER_SUCCESS = 'DELETE_MASTER_SUCCESS';
export const DELETE_MASTER_FAILURE = 'DELETE_MASTER_FAILURE';

export const UPLOAD_MASTER_IMAGE_REQUEST = 'UPLOAD_MASTER_IMAGE_REQUEST';
export const UPLOAD_MASTER_IMAGE_SUCCESS = 'UPLOAD_MASTER_IMAGE_SUCCESS';
export const UPLOAD_MASTER_IMAGE_FAILURE = 'UPLOAD_MASTER_IMAGE_FAILURE';

export const DELETE_MASTER_IMAGE_REQUEST = 'DELETE_MASTER_IMAGE_REQUEST';
export const DELETE_MASTER_IMAGE_SUCCESS = 'DELETE_MASTER_IMAGE_SUCCESS';
export const DELETE_MASTER_IMAGE_FAILURE = 'DELETE_MASTER_IMAGE_FAILURE';

export const CHANGE_PAGE = 'CHANGE_PAGE';

export const CHANGE_FORM = 'CHANGE_FORM';
export const RESET_FORM = 'RESET_FORM';

export const changeForm = (name, value) => ({
  type: CHANGE_FORM,
  payload: {name: name, value: value},
})

export function resetForm(resetId = false) {
  return (dispatch) => {
    dispatch({ type: RESET_FORM, payload: resetId });
    dispatch({ type: "RESET_VALIDATION_ERRORS" });
  };
}

export function changePage(newPage) {
  return (dispatch) => {
    this.fetchMastersList(newPage);

    dispatch({
      type: CHANGE_PAGE,
      payload: newPage
    });
  }
}

export function fetchMastersList(current_page = 1, per_page = 10) {
  return (dispatch) => {
    dispatch({ type: GET_MASTERS_REQUEST });
    return axios.get(`/api/admin/masters?page=${current_page}&per_page=${per_page}`)
      .then(response => dispatch({ type: GET_MASTERS_SUCCESS, payload: response.data }) )
      .catch(error => dispatch({ type: GET_MASTERS_FAILURE, payload: error, error: true }) );
  }
}

export function fetchMaster(id) {
  return (dispatch) => {
    dispatch({ type: GET_MASTER_REQUEST });
    return axios.get(`api/admin/masters/${id}`)
      .then(response => dispatch({ type: GET_MASTER_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_MASTER_FAILURE, payload: error, error: true }) );
  }
}

export function createMaster() {
  return (dispatch) => {
    dispatch({ type: CREATE_MASTER_SUCCESS });
  }
}

export function storeMaster(data, afterSuccess = null) {
  return (dispatch) => {
    dispatch({ type: STORE_MASTER_REQUEST });
    return axios.post('api/admin/masters', data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: STORE_MASTER_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: STORE_MASTER_FAILURE, payload: error.response }) );
  }
}

export function editMaster(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: EDIT_MASTER_REQUEST });
    return axios.get(`api/admin/masters/${id}`)
      .then(response => {
        const resultData = response.data.data;
        dispatch({type: EDIT_MASTER_SUCCESS, payload: resultData});

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: EDIT_MASTER_FAILURE, payload: error.response }) );
  }
}

export function updateMaster(id, data, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: UPDATE_MASTER_REQUEST });
    return axios.put(`api/admin/masters/${id}`, data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: UPDATE_MASTER_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: UPDATE_MASTER_FAILURE, payload: error.response }) );
  }
}

export function deleteMaster(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: DELETE_MASTER_REQUEST });
    return axios.delete(`api/admin/masters/${id}`)
      .then(response => {
        dispatch({ type: DELETE_MASTER_SUCCESS, payload: id });

        if (typeof afterSuccess === 'function')
          afterSuccess();
      })
      .catch(error => dispatch({ type: DELETE_MASTER_FAILURE, payload: error.response }) );
  }
}

export function uploadMasterImage(file, afterSuccess) {
  return (dispatch) => {
    const formData = new FormData();
    formData.append('file', file);

    dispatch({ type: UPLOAD_MASTER_IMAGE_REQUEST });
    return axios.post('api/admin/images/master-image', formData)
      .then(response => {
        dispatch({ type: UPLOAD_MASTER_IMAGE_SUCCESS, payload: response.data.data });

        if (typeof afterSuccess === 'function')
          afterSuccess();
      })
      .catch(error => dispatch({ type: UPLOAD_MASTER_IMAGE_FAILURE, payload: error.response }) );
  }
}

export function deleteMasterImage(afterSuccess) {
  return (dispatch) => {
    dispatch({ type: DELETE_MASTER_IMAGE_SUCCESS });
  }
}
