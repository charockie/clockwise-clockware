import axios from 'axios';

export const GET_CITIES_LIST_REQUEST = 'GET_CITIES_LIST_REQUEST';
export const GET_CITIES_LIST_SUCCESS = 'GET_CITIES_LIST_SUCCESS';
export const GET_CITIES_LIST_FAILURE = 'GET_CITIES_LIST_FAILURE';

export const GET_CITIES_REQUEST = 'GET_CITIES_REQUEST';
export const GET_CITIES_SUCCESS = 'GET_CITIES_SUCCESS';
export const GET_CITIES_FAILURE = 'GET_CITIES_FAILURE';

export const GET_CITY_REQUEST = 'GET_CITY_REQUEST';
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS';
export const GET_CITY_FAILURE = 'GET_CITY_FAILURE';

export const STORE_CITY_REQUEST = 'STORE_CITY_REQUEST';
export const STORE_CITY_SUCCESS = 'STORE_CITY_SUCCESS';
export const STORE_CITY_FAILURE = 'STORE_CITY_FAILURE';

export const EDIT_CITY_REQUEST = 'EDIT_CITY_REQUEST';
export const EDIT_CITY_SUCCESS = 'EDIT_CITY_SUCCESS';
export const EDIT_CITY_FAILURE = 'EDIT_CITY_FAILURE';

export const UPDATE_CITY_REQUEST = 'UPDATE_CITY_REQUEST';
export const UPDATE_CITY_SUCCESS = 'UPDATE_CITY_SUCCESS';
export const UPDATE_CITY_FAILURE = 'UPDATE_CITY_FAILURE';

export const DELETE_CITY_REQUEST = 'DELETE_CITY_REQUEST';
export const DELETE_CITY_SUCCESS = 'DELETE_CITY_SUCCESS';
export const DELETE_CITY_FAILURE = 'DELETE_CITY_FAILURE';

export const CHANGE_PAGE = 'CHANGE_PAGE';

export const CHANGE_FORM = 'CHANGE_FORM';
export const RESET_FORM = 'RESET_FORM';

export const changeForm = (name, value) => ({
  type: CHANGE_FORM,
  payload: {name: name, value: value},
})

export function resetForm(resetId = false) {
  return (dispatch) => {
    dispatch({ type: RESET_FORM, payload: resetId });
    dispatch({ type: "RESET_VALIDATION_ERRORS" });
  };
}

export function changePage(newPage) {
  return (dispatch) => {
    this.fetchCitiesList(newPage);

    dispatch({
      type: CHANGE_PAGE,
      payload: newPage
    });
  }
}

export function fetchCitiesShortList() {
  return (dispatch) => {
    dispatch({ type: GET_CITIES_LIST_REQUEST });
    return axios.get(`/api/admin/cities/list`)
      .then(response => dispatch({ type: GET_CITIES_LIST_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_CITIES_LIST_FAILURE, payload: error, error: true }) );
  }
}

export function fetchCitiesList(current_page = 1, per_page = 10) {
  return (dispatch) => {
    dispatch({ type: GET_CITIES_REQUEST });
    return axios.get(`/api/admin/cities?page=${current_page}&per_page=${per_page}`)
      .then(response => dispatch({ type: GET_CITIES_SUCCESS, payload: response.data }) )
      .catch(error => dispatch({ type: GET_CITIES_FAILURE, payload: error, error: true }) );
  }
}

export function fetchCity(id) {
  return (dispatch) => {
    dispatch({ type: GET_CITY_REQUEST });
    return axios.get(`api/admin/cities/${id}`)
      .then(response => dispatch({ type: GET_CITY_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_CITY_FAILURE, payload: error, error: true }) );
  }
}

export function storeCity(data, afterSuccess = null) {
  return (dispatch) => {
    dispatch({ type: STORE_CITY_REQUEST });
    return axios.post('api/admin/cities', data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: STORE_CITY_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: STORE_CITY_FAILURE, payload: error.response }) );
  }
}

export function editCity(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: EDIT_CITY_REQUEST });
    return axios.get(`api/admin/cities/${id}`)
      .then(response => {
        const resultData = response.data.data;
        dispatch({type: EDIT_CITY_SUCCESS, payload: resultData});

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: EDIT_CITY_FAILURE, payload: error.response }) );
  }
}

export function updateCity(id, data, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: UPDATE_CITY_REQUEST });
    return axios.put(`api/admin/cities/${id}`, data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: UPDATE_CITY_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: UPDATE_CITY_FAILURE, payload: error.response }) );
  }
}

export function deleteCity(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: DELETE_CITY_REQUEST });
    return axios.delete(`api/admin/cities/${id}`)
      .then(response => {
        dispatch({ type: DELETE_CITY_SUCCESS, payload: id });

        if (typeof afterSuccess === 'function')
          afterSuccess();
      })
      .catch(error => dispatch({ type: DELETE_CITY_FAILURE, payload: error.response }) );
  }
}
