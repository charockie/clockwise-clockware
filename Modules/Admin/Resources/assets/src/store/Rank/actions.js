import axios from 'axios';

export const GET_RANKS_LIST_REQUEST = 'GET_RANKS_LIST_REQUEST';
export const GET_RANKS_LIST_SUCCESS = 'GET_RANKS_LIST_SUCCESS';
export const GET_RANKS_LIST_FAILURE = 'GET_RANKS_LIST_FAILURE';

export function fetchRanksList() {
  return (dispatch) => {
    dispatch({ type: GET_RANKS_LIST_REQUEST });
    return axios.get('/api/admin/ranks/list')
      .then(response => dispatch({ type: GET_RANKS_LIST_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_RANKS_LIST_FAILURE, payload: error, error: true }) );
  }
}
