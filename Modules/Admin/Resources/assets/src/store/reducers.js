import { combineReducers } from 'redux';
import { loginReducer } from '../store/Login/reducers';
import { cityReducer } from './City/reducers';
import { masterReducer } from './Master/reducers';
import { rankReducer } from './Rank/reducers';
import { loadingReducer } from './api/loadingReducer';
import { validationErrorsReducer } from './api/validationErrorsReducer';
import { customerReducer } from './Customer/reducers';
import { orderReducer } from './Order/reducers';

export default combineReducers({
  login: loginReducer,
  city: cityReducer,
  master: masterReducer,
  customer: customerReducer,
  rank: rankReducer,
  order: orderReducer,
  api: combineReducers({
    loading: loadingReducer,
    validationErrors: validationErrorsReducer,
  }),
});
