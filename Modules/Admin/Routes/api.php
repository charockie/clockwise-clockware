<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(["auth:api", "cors"])
    ->namespace("Api")
    ->group(function () {

        Route::get("user", "UserController@showCurrent")->name("users.current");
        Route::name("cities.")->prefix("cities")->group(function () {
            Route::get("list", "CityController@shortList")->name("list");
        });
        Route::resource("cities", "CityController")->except(["create", "edit"]);
        Route::resource("masters", "MasterController")->except(["create", "edit"]);
        Route::resource("customers", "CustomerController")->except(["create", "store", "edit"]);
        Route::resource("orders", "OrderController")->except(["create", "store", "edit"]);
        Route::name("images.")->prefix("images")->group(function () {
            Route::post("master-image", "ImageController@storeMasterImage")->name("store.master-image");
            Route::delete("{image}", "ImageController@destroy")->name("destroy");
        });
        Route::name("ranks.")->prefix("ranks")->group(function () {
            Route::get("list", "RankController@shortList")->name("list");
        });

    });
