<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name("admin.")->prefix("admin")->middleware("cors")->group(function () {
  Route::auth(["register" => false, "verify" => false]);
});

Route::fallback("AdminController@index");
