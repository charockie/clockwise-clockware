<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name("api.")->namespace("Api")->group(function () {

    Route::name("landing.")->group(function () {
        Route::get("stats", "StatController@index")->name("stats");
        Route::get("pricing-plans", "PlanController@index")->name("pricing-plans");
        Route::get("clocks", "ClockController@index")->name("clocks");
        Route::get("cities", "CityController@index")->name("cities");
        Route::get("masters", "MasterController@index")->name("masters");
    });

});
